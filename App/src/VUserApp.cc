#include "../include/VUserApp.hh"

VUserApp::VUserApp(const std::string &name) : SV::VAppManager()
{
    this->SetName(name);
}

VUserApp::~VUserApp()
{

}

void VUserApp::ProcessUserApp()
{
    Process();
}

void VUserApp::UnloadUserApp()
{
    Unload();
    UnloadUI();
}

void VUserApp::LoadUserApp(SV::GUI::ViewOpenGL *viewWin, SV::GUI::VMainWindow *mainWin)
{
    fpViewOpenGL = viewWin;
    fpMainWindow = mainWin;

    Load();
    LoadUI();
}

void VUserApp::AddDummyChildsToWorld()
{
    using namespace Models::DNA::Molecular;

}

void VUserApp::RemoveDummyChilds()
{
    using namespace Models::DNA::Molecular;

    if(fpDummy != nullptr)
    {
        SV::Run::VSceneNode* worldNode = fpWorldNode;

        // Loop on all the node children
        for(int i=0, ie=worldNode->GetNumOfChildren(); i<ie; i++)
        {
            // If the child is the dummy, delete it
            if(worldNode->GetChild(i) == fpDummy)
            {
                worldNode->RemoveChild(i);
                fpDummy = nullptr;
                break; // Interrupt the loop
            }
        }
    }
}
