#include "../include/AppManager.hh"

#include <QPushButton>
#include <QDockWidget>
#include "GUI/Management/include/MainWindow.hh"

using namespace SV;

AppManager::AppManager(const std::string& appName) : VAppManager()
{
    SetName(appName);

    fCurrentAppIndex = 0;

    //VUserApp* homeApp = new HomeApp("Home");
    //AddApp(homeApp);

    VUserApp* homeApp = new HomeApp("HomeApp");
    AddApp(homeApp);
}

AppManager::~AppManager()
{

}

void AppManager::Load()
{
    MainWindow* mainWin = dynamic_cast<MainWindow*>(fpMainWindow);

    // Just load the current app
    GetCurrentApp()->LoadUserApp(fpViewOpenGL, fpMainWindow);

    SV::GUI::Camera* camera = GetCurrentApp()->GetCamera();

    // Initialize the values of the camera position and orientation in the UI
    fPos = camera->GetPosition();
    mainWin->SetPositionDisplay(fPos);
    fOri = camera->GetOrientation();
    mainWin->SetOrientationDisplay(fOri);
}

void AppManager::Unload()
{
    // Uninitialize the application
    GetCurrentApp()->UnloadUserApp();

}

void AppManager::Process()
{
    // Called at each main loop iteration.
    // All the content of this method is critical for performance.

    GetCurrentApp()->ProcessUserApp();

    MainWindow* mainWin (static_cast<MainWindow*>(fpMainWindow) );
    SV::GUI::Camera* camera = GetCurrentApp()->GetCamera();

    // Update Camera pos on the UI
    if(fPos != camera->GetPosition() )
    {
        fPos = camera->GetPosition();
        mainWin->SetPositionDisplay(fPos);
    }

    // Update Camera orientation on the UI
    if(fOri != camera->GetOrientation() )
    {
        fOri = camera->GetOrientation();
        mainWin->SetOrientationDisplay(fOri );
    }
}

SV::Run::VSceneNode* AppManager::GetWorldNode() const
{

}

void AppManager::SelectApp(int /*index*/)
{

}

void AppManager::AddApp(VUserApp *app)
{
    fApps.push_back(app);
}

VUserApp* AppManager::GetApp(unsigned int index) const
{
    if(index < fApps.size() )
        return fApps[index];
    else
    {
        Utils::Exception(Utils::ExceptionType::Fatal,
                         "AppManager::GetApp",
                         "The app could not be fetched because of an out of bound index.");
    }
}

