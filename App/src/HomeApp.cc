#include "../include/HomeApp.hh"

#include "GUI/Management/include/MainWindow.hh"
#include "ui_MainWindow.h"
#include <QFileDialog>

HomeApp::HomeApp(const std::string &name) : QObject(nullptr), VUserApp(name)
{
    fpVoxelStraight = nullptr;
}

HomeApp::~HomeApp()
{

}

void HomeApp::Load()
{
    // Initialize the application

    using namespace Models::DNA::Molecular;

    // *********************************
    // World root node creation
    // *********************************

    fpWorldNode = new SV::Run::SceneNode();
    fpWorldNode->SetName("World");
    fpWorldNode->AddComponent(new SV::Logic::Renderable(), SV::Logic::List::Renderable);

    fpVoxelStraight = new VoxelStraight("Straight voxel");
    fpVoxelStraight->Generate();

    fpWorldNode->AddChild(fpVoxelStraight);

    // *********************************
    // Camera creation
    // *********************************

    fpCamera = new SV::GUI::Camera(QVector3D(0,0,-30), QVector3D(0,10,0), QVector3D(0,1,0), 1., 5);
    fpCameraStyle = new SV::GUI::GrabStyle("grabStyle", fpViewOpenGL, fpCamera);

    // Standad camera configuration to look at a simple nucleosome
    fpCamera->SetPosition(QVector3D(153, 1.33f, 77) );
    fpCamera->SetViewDirection(QVector3D(-0.9f,0.2f,-0.3f) );
    fpCamera->SetSensitivity(0.5);
    fpCamera->SetSpeed(2);//100.5);

    // *********************************
    // 3D render system creation and initialization
    // *********************************

    fpSysRender = new SV::Logic::Render();

    // Light definition
    //
    SV::TriD::LightManager* lightManager = fpSysRender->GetLightManager();
    SV::TriD::DirectionalLight* dirLight = new SV::TriD::DirectionalLight();
    // Ambient, diffuse, specular
    dirLight->SetDirection(QVector3D(0,0,-1) );
    dirLight->SetLight(QVector3D(0.4f, 0.4f, 0.4f), QVector3D(0.4f, 0.4f, 0.4f), QVector3D(0.1f, 0.1f, 0.1f) );
    lightManager->AddDirLight("SunLight", dirLight);

    // Addition of a skybox
    //
    SV::TriD::SkyBox* skyBox = new SV::TriD::SkyBox(fpCamera,
                                     "Data/Assets/Skybox/water_montain/left.jpg",
                                     "Data/Assets/Skybox/water_montain/right.jpg",
                                     "Data/Assets/Skybox/water_montain/top.jpg",
                                     "Data/Assets/Skybox/water_montain/bottom.jpg",
                                     "Data/Assets/Skybox/water_montain/back.jpg",
                                     "Data/Assets/Skybox/water_montain/front.jpg");

    fpSysRender->SetVerbose(1);
    fpSysRender->SetView(fpViewOpenGL);
    fpSysRender->SetCamera(fpCamera);
    fpSysRender->SetWorld(fpWorldNode);
    fpSysRender->SetSkyBox(skyBox);
    //fpViewOpenGL->SetBackgroundColor(QColor(255,255,255));

    // ********************************

    // Initialization of the render system
    fpSysRender->Initialize();

    this->Reload3dObjects();

}

void HomeApp::Unload()
{
    delete fpSysRender;
    delete fpCamera;
    delete fpCameraStyle;
    delete fpWorldNode;

    // fpVoxelStraight was deleted when the world was destroyed
    fpVoxelStraight = nullptr;
}



void HomeApp::Process()
{
    // Called at each main loop iteration.
    // All the content of this method is critical for performance.

    fpViewOpenGL->UpdateInput();

    fpCameraStyle->Try(fpViewOpenGL->GetInput() );

    fpSysRender->Process();
}

void HomeApp::LoadUI()
{
    MainWindow* mainWin = dynamic_cast<MainWindow*>(fpMainWindow);

    // ********************************
    // Position and orientation
    // ********************************

    QObject::connect(mainWin->GetUI()->posXEdit, &QLineEdit::editingFinished,
                     this, &HomeApp::UpdateCameraPos);
    QObject::connect(mainWin->GetUI()->posYEdit, &QLineEdit::editingFinished,
                     this, &HomeApp::UpdateCameraPos);
    QObject::connect(mainWin->GetUI()->posZEdit, &QLineEdit::editingFinished,
                     this, &HomeApp::UpdateCameraPos);

    QObject::connect(mainWin->GetUI()->oriXEdit, &QLineEdit::editingFinished,
                     this, &HomeApp::UpdateCameraPos);
    QObject::connect(mainWin->GetUI()->oriYEdit, &QLineEdit::editingFinished,
                     this, &HomeApp::UpdateCameraPos);
    QObject::connect(mainWin->GetUI()->oriZEdit, &QLineEdit::editingFinished,
                     this, &HomeApp::UpdateCameraPos);


    // ********************************
    // Export to Geant4
    // ********************************

    QObject::connect(mainWin->GetUI()->export_g4_button, &QToolButton::clicked,
                     this, &HomeApp::ExportToG4DNA);
}

void HomeApp::UnloadUI()
{
    MainWindow* mainWin = dynamic_cast<MainWindow*>(fpMainWindow);

    // ********************************
    // Position and orientation
    // ********************************

    QObject::disconnect(mainWin->GetUI()->posXEdit, &QLineEdit::editingFinished,
                     this, &HomeApp::UpdateCameraPos);
    QObject::disconnect(mainWin->GetUI()->posYEdit, &QLineEdit::editingFinished,
                     this, &HomeApp::UpdateCameraPos);
    QObject::disconnect(mainWin->GetUI()->posZEdit, &QLineEdit::editingFinished,
                     this, &HomeApp::UpdateCameraPos);

    QObject::disconnect(mainWin->GetUI()->oriXEdit, &QLineEdit::editingFinished,
                     this, &HomeApp::UpdateCameraPos);
    QObject::disconnect(mainWin->GetUI()->oriYEdit, &QLineEdit::editingFinished,
                     this, &HomeApp::UpdateCameraPos);
    QObject::disconnect(mainWin->GetUI()->oriZEdit, &QLineEdit::editingFinished,
                     this, &HomeApp::UpdateCameraPos);

    // ********************************

}

void HomeApp::Reload3dObjects()
{
    fpSysRender->Reload3DObjects();
}

// ********************************
// Slots
// ********************************

void HomeApp::ExportToG4DNA()
{
    QFileDialog fileDialog;
    fileDialog.setOption(QFileDialog::Option::DontUseNativeDialog, true);
    fileDialog.setFileMode(QFileDialog::AnyFile);
    fileDialog.setWindowTitle("Save as");
    fileDialog.setNameFilter(tr("DnaFabric export to G4 file (*.fab2g4dna)") );

    if(fileDialog.exec() && fileDialog.selectedFiles().size()>0)
    {
        QString filePath = fileDialog.selectedFiles()[0];
        QFileInfo fInfo (filePath);

        // Get the line edit field value
        std::string fileName = filePath.toStdString();

        std::stringstream fileNameSS;
        fileNameSS<<fileName;

        // Check the extension is correct
        if(fInfo.suffix() != "fab2g4dna")
        {
            // Add the extension to the file name
            fileNameSS<<".fab2g4dna";
        }

        Export::Geant4::ExportVoxel exportVoxel(fpVoxelStraight);

        std::cout<<"Start export into "<<fileNameSS.str()<<std::endl;
        exportVoxel.Export(fileNameSS.str() );
    }
}

void HomeApp::UpdateCameraPos()
{
    MainWindow* mainWin = static_cast<MainWindow*>(fpMainWindow);

    float x = mainWin->GetUI()->posXEdit->text().toFloat();
    float y = mainWin->GetUI()->posYEdit->text().toFloat();
    float z = mainWin->GetUI()->posZEdit->text().toFloat();

    float ox = mainWin->GetUI()->oriXEdit->text().toFloat();
    float oy = mainWin->GetUI()->oriYEdit->text().toFloat();
    float oz = mainWin->GetUI()->oriZEdit->text().toFloat();

    QVector3D pos(x, y, z);
    QVector3D ori(ox, oy, oz);

    if(pos != fpCameraStyle->GetCamera()->GetPosition() )
        fpCameraStyle->GetCamera()->SetPosition(pos);

    if(ori != fpCameraStyle->GetCamera()->GetOrientation() )
        fpCameraStyle->GetCamera()->SetOrientation(ori);
}

