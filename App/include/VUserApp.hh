#ifndef VUSERAPP_HH
#define VUSERAPP_HH

#include "SymVox/Run/Scene/Graph/include/SceneNode.hh"
#include "Logic/BioStructures/Cell/DNA/Molecular/include/Nucleosome.hh"
#include "Logic/BioStructures/Cell/DNA/Molecular/include/Linker.hh"
#include "SymVox/GUI/UserAction/CameraStyle/include/GrabStyle.hh"
#include "Logic/BioStructures/Cell/DNA/Voxels/include/VoxelStraight.hh"

#include "SymVox/App/Management/include/VAppManager.hh"

/**
 * @brief The VUserApp class represents a DnaFabric user-application managed by the AppManager.
 *
 * A DnaFabric user-application is very similar to a classic AppManager because it replicates its structure : load, unload, process.
 * However, it adds a few new methods, such as LoadUI and UnloadUI, that facilitate the creation of new DnaFabric user-application.
 * In the end, any DnaFabric user-application should inherit from this VUserApp class and implements it pure virtual methods.
 */
class VUserApp : public SV::VAppManager
{
public:
    VUserApp(const std::string& name);
    ~VUserApp();

    /**
     * @brief To get a pointer to the world VSceneNode.
     * @return A pointer to the world VSceneNode
     */
    virtual SV::Run::VSceneNode* GetWorldNode() {return fpWorldNode;}

    void LoadUserApp(SV::GUI::ViewOpenGL* viewWin,
              SV::GUI::VMainWindow* mainWin);
    ///< @brief LoadUserApp method used to initialize the application.

    void ProcessUserApp(); ///< @brief LoadUserApp method is called at each frame.
    void UnloadUserApp(); ///< @brief UnloadUserApp method used to uninitialize the application.

    SV::GUI::Camera* GetCamera() const {return fpCamera;} ///< @brief Return a pointer to the camera

    /**
     * @brief Add "dummy" objects into the world.
     *
     * Dummy objects that are only meant to trigger the loading of the 3D-LODs associated with them.
     * As such, the dummy objects should be created before the initialization of the render system and deleted once it is complete.
     */
    virtual void AddDummyChildsToWorld();

    /**
     * @brief Remove "dummy" objects from the world.
     *
     * Dummy objects that are only meant to trigger the loading of the 3D-LODs associated with them.
     * As such, the dummy objects should be created before the initialization of the render system and deleted once it is complete.
     */
    virtual void RemoveDummyChilds();

    virtual void Reload3dObjects() = 0;

protected:
    /**
     * @brief SymVox defaut Camera class that implements its own user actions.
     *
     * The default SymVox camera implements its own user action which means it can be moved through a pre-defined set of keys.
     * For more information see AppManager::fpCameraStyle.
     *
     */
    SV::GUI::Camera* fpCamera;

    /**
     * @brief The root node of the simulated world.
     *
     * This is the root node of the scene graph used by the application.
     */
    SV::Run::VSceneNode* fpWorldNode;

    /**
     * @brief The root node of all dummy objects.
     *
     * Dummy objects are used to force the loading of 3D models that may not be present in the initial world.
     * For example, they may be added to the world when a simulation is run and, to be able to see them, we need
     * to load them.
     */
    SV::Run::VSceneNode* fpDummy;

    /**
     * @brief LoadUI allows to customize the interface for the user-application
     */
    virtual void LoadUI() {}

    /**
     * @brief UnloadUI should be used to undo the customization of LoadUI to make the UI go back to its original state.
     */
    virtual void UnloadUI() {}

    virtual void Load() = 0; ///< @brief Load method used to initialize the application.
    virtual void Unload() = 0; ///< @brief Unload method used to uninitialize the application.
    virtual void Process() = 0; ///< @brief Process method called at each main loop iteration to run SymVox systems.
};

#endif // VUSERAPP_HH
