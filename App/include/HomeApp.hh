#ifndef HOMEAPP_HH
#define HOMEAPP_HH

#include "SymVox/Logic/System/Render/include/Render.hh"
#include "SymVox/Run/Scene/Graph/include/SceneNode.hh"
#include "SymVox/3D/Geometry/include/Sphere.hh"
#include "Logic/Export/Geant4/DNA/include/ExportVoxel.hh"
#include "Logic/BioStructures/Cell/DNA/Voxels/include/VoxelStraight.hh"
#include <QObject>

#include "VUserApp.hh"

/**
 * @brief The HomeApp class defines the home user application.
 *
 * The HomeApp class defines the home user application that is shown at startup. A user should see it before
 * anything else. Then, DnaFabric UI will allow the user to select another application better suited to its needs.
 */
class HomeApp : public QObject, public VUserApp
{
    Q_OBJECT

public:
    HomeApp(const std::string& name);
    ~HomeApp() override;

    void Load() override final; ///< @brief Load method used to initialize the application.
    void Unload() override final; ///< @brief Unload method used to uninitialize the application.
    void Process() override final; ///< @brief Process method called at each main loop iteration to run SymVox systems.

    void LoadUI() override final; ///< @brief Load the specific UI of the app
    void UnloadUI() override final; ///< @brief Unload the specific UI of the app

    void Reload3dObjects() override final; ///< @brief Trigger a realoading of the 3D objects of the world

private:
    /**
     * @brief The camera style used by the application.
     *
     * Two mods (or styles) are available with two different behaviors:
     *  - GrabStyle will hide the mouse pointer when the "F" key is pressed and use mouse movements to update the camera orientation. The following keboard keys can be pressed:
     *      + "Z" to go forward.
     *      + "Q" to go to the left.
     *      + "D" to go to the right.
     *      + "S" to go backward.
     *      .
     *  At any time, another pressed of the "F" key will give the mouse back to the user.
     *  - MouseStyle implements a way to use the camera base uniquely on the mouse:
     *      + "Left click" (stay pressed) to move the direction of the camera.
     *      + "Right click" (stay pressed) to translate the camera.
     */
    SV::GUI::VMoveStyle* fpCameraStyle;

    /**
     * @brief Pointer to the system in charge of the 3D rendering of the world.
     *
     * This system is a part of SymVox and, thus, it should be initialized, processed and uninitialized
     * in the Load(), Process() and Unload() application methods.
     */
    SV::Logic::Render* fpSysRender;

    int fSimuTabIndex; ///< @brief The index of the Simu tab

    Models::DNA::Molecular::VoxelStraight* fpVoxelStraight;

private slots:
    void ExportToG4DNA(); ///< @brief Export the first daughter volume, that should be a cell, to a G4DNA compatible file.
    void UpdateCameraPos(); ///< @brief Update the position of the camera.
};

#endif // HOMEAPP_HH
