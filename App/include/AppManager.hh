#ifndef APPMANAGER_HH
#define APPMANAGER_HH

#include "SymVox/Logic/System/Render/include/Render.hh"
#include "SymVox/GUI/UserAction/CameraStyle/include/GrabStyle.hh"
#include "SymVox/Run/Scene/Graph/include/SceneNode.hh"
#include "SymVox/3D/Geometry/include/Sphere.hh"
#include "Logic/BioStructures/Cell/DNA/Molecular/include/Nucleosome.hh"
#include "Logic/BioStructures/Cell/DNA/Molecular/include/Linker.hh"
#include "Logic/BioStructures/Cell/DNA/Voxels/include/VoxelStraight.hh"
#include "HomeApp.hh"
#include "VUserApp.hh"

#include "SymVox/App/Management/include/VAppManager.hh"

/**
 * @brief The AppManager class is the SymVox application.
 *
 * Any SymVox application should have an AppManager and be built around it.
 * Through inheritance, any AppManager should implement the virtual methods named Load(), Unload() and Process().
 * These will be called by the SymVox RunManager in the following order:
 *  -# Load() to initialize the SymVox appliction: 3D object creation, world structure, GUI, camera...
 *  -# Process() is called at each main loop iteration (usually around 30-60 times per second) and should call the SymVox systems required by the application.
 *  -# Unload() clean the application before shutdown, it should deallocate memory, set pointer to zero and so on.
 * It is important to take into account the Load() and  Unload() methods as any application built as a combination of several others will rely heavily on them.
 *
 * In DnaFabric, the AppManager is used to manage several user-applications (VUserApp). These can be switch on and off when the
 * program is running. The DnaFabric user-applications contain most of the code classicaly placed in the AppManager: load and
 * unload logic, process method etc.
 */
class AppManager : public SV::VAppManager
{
public:
    AppManager(const std::string& appName); ///< @brief Constructor
    ~AppManager() override; ///< @brief Destructor

    void Load() override final; ///< @brief Load method used to initialize the application.
    void Unload() override final; ///< @brief Unload method used to uninitialize the application.
    void Process() override final; ///< @brief Process method called at each main loop iteration to run SymVox systems.

    SV::Run::VSceneNode* GetWorldNode() const; ///< @brief Return the root node used to represent the world

    void SelectApp(int index); ///< @brief Load the app related to the index and Unload any previoulsy loaded app.
    void AddApp(VUserApp* app); ///< @brief Register a new App in the program

    VUserApp* GetCurrentApp() const {return fApps[fCurrentAppIndex];} ///< @brief Return the currently activated app
    VUserApp* GetApp(unsigned int index) const; ///< @brief Return the app related to the given index.

private:
    unsigned int fCurrentAppIndex; ///< @brief Index of the current app
    std::vector<VUserApp*> fApps; ///< @brief List of all the registered apps.

    QVector3D fPos; ///< @brief A variable to store the position of the camera
    QVector3D fOri; ///< @brief A variable to store the position of the camera
};

#endif // APPMANAGER_HH
