#include "../include/MainWindow.hh"
#include "ui_MainWindow.h"

MainWindow::MainWindow(AppManager &appManager, QWidget *parent) :
    SV::GUI::VMainWindow(parent),
    ui(new Ui::MainWindow),
    fAppManager(appManager)
{
    ui->setupUi(this);

    setWindowState(Qt::WindowMaximized);

    ui->centralwidget->layout()->setContentsMargins(0, 0, 0, 0);
    statusBar()->hide();

    QIcon icon(":/Data/logo.png");
    setWindowIcon(icon);

    // Start with the first tab selected
    ui->tabWidget->setCurrentIndex(0);

    // *******************************
    // UI signals/slots part
    // *******************************

    // FullScreen trigger
    connect(ui->fullscreen_button, &QToolButton::clicked,
            this, &MainWindow::triggerWindowMode);
    connect(ui->actionFullScreen, &QAction::triggered,
            this, &MainWindow::triggerWindowMode);
}

MainWindow::~MainWindow()
{
    delete ui;

    if(fpViewOpenGL != nullptr)
        delete fpViewOpenGL;
}

void MainWindow::Initialise()
{
    if(!fIsInit)
    {
        if(fpViewOpenGL == nullptr)
            fpViewOpenGL = new SV::GUI::ViewOpenGL();

        // Add a grid layout to the widget placed in the mainWindow
        QGridLayout* gridLayout = new QGridLayout();
        ui->view_widget->setLayout(gridLayout);
        // No margins to get full screen display
        gridLayout->setContentsMargins(0,0,0,0);

        // Create the window contained widget linked to the openGL window
        QWidget* openGLContainer = QWidget::createWindowContainer(fpViewOpenGL);
        openGLContainer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

        // Add the window container widget to the grid layout
        gridLayout->addWidget(openGLContainer);

        // Remove the useless view tab
        ui->tabWidget->removeTab(4);

        fIsInit = true;
    }
    else
    {
        SV::Utils::Exception(SV::Utils::ExceptionType::Fatal, "MainWindow::Initialise",
                         "Already initialised");
    }
}

void MainWindow::triggerWindowMode()
{
    if(this->ui->tabWidget->isVisible() )
    {
        this->ui->tabWidget->setVisible(false);
        this->ui->actionFullScreen->setChecked(true);
    }
    else
    {
        this->ui->tabWidget->setVisible(true);
        this->ui->actionFullScreen->setChecked(false);
    }
}

void MainWindow::SetPositionDisplay(const QVector3D& pos)
{
    // This condition is required to avoid a segfault
    QString str;

    str = QString::number(double(pos[0] ) );
    ui->posXEdit->setText(str);

    str = QString::number(double(pos[1] ) );
    ui->posYEdit->setText(str);

    str = QString::number(double(pos[2] ) );
    ui->posZEdit->setText(str);
}

void MainWindow::SetOrientationDisplay(const QVector3D& pos)
{
    QString str;

    str = QString::number(double(pos[0] ) );
    ui->oriXEdit->setText(str);

    str = QString::number(double(pos[1] ) );
    ui->oriYEdit->setText(str);

    str = QString::number(double(pos[2] ) );
    ui->oriZEdit->setText(str);
}

QTabWidget *MainWindow::GetTabWidget()
{
    return ui->tabWidget;
}

QWidget *MainWindow::GetViewWidget()
{
    return ui->view_widget;
}
