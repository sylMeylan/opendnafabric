#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

#include <QGridLayout>

#include "SymVox/GUI/Windows/include/VMainWindow.hh"
#include "App/include/AppManager.hh"

namespace Ui {
class MainWindow;
}

/**
 * @brief The MainWindow class define custom main window for DnaFabric
 *
 * This class uses the virtual main window provided by SymVox. The virtual base class required
 * the implementation of :
 * -# An Initialize() function where the ViewOpenGL widget should be created and placed in the main window.
 */
class MainWindow : public SV::GUI::VMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(AppManager &appManager, QWidget *parent = 0);
    ~MainWindow() override;

    void Initialise() override final;

    void SetPositionDisplay(const QVector3D &pos);
    void SetOrientationDisplay(const QVector3D &pos);

    QTabWidget* GetTabWidget();
    QWidget* GetViewWidget();
    Ui::MainWindow* GetUI() const {return ui;}

private:
    Ui::MainWindow *ui;
    AppManager& fAppManager;

private slots:
    void triggerWindowMode();
};

#endif // MAINWINDOW_HH
