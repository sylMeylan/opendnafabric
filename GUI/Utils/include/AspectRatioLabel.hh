#ifndef ASPECTRATIOLABEL_HH
#define ASPECTRATIOLABEL_HH

#include <QLabel>

namespace Utils {

/**
 * @brief The AspectRatioLabel class is useful to show an image that will automatically resize itself and keep
 * its proportions.
 *
 * This class can be use as it is in a hard coded interface or through the QtDesigner by promoted a QLabel item to
 * an AspectRatioLabel. In the latter case, do not forget to take into account the Utils namespace.
 */
class AspectRatioLabel : public QLabel
{
public:
    explicit AspectRatioLabel(QWidget* parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
    ~AspectRatioLabel() override;

public slots:
    void setPixmap(const QPixmap& pm);

protected:
    void resizeEvent(QResizeEvent* event) override;

private:
    void updateMargins();

    int pixmapWidth = 0;
    int pixmapHeight = 0;
};

}

#endif // ASPECTRATIOLABEL_HH
