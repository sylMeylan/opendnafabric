#ifndef SWITCHBUTTON_HH
#define SWITCHBUTTON_HH

#include <QtWidgets>

/**
 * @brief The SwitchButton class defines a custom widget: a button that can be set to on or off.
 */
class SwitchButton : public QAbstractButton {
    Q_OBJECT
    Q_PROPERTY(int offset READ offset WRITE setOffset)
    Q_PROPERTY(QBrush brush READ brush WRITE setBrush)

public:
    SwitchButton(QWidget* parent = nullptr);
    SwitchButton(const QBrush& brush, QWidget* parent = nullptr);

    QSize sizeHint() const override;

    QBrush brush() const {
        return _brush;
    }
    void setBrush(const QBrush &brsh) {
        _brush = brsh;
    }

    int offset() const {
        return _x;
    }
    void setOffset(int o) {
        _x = o;
        update();
    }

    bool isToggled() const {return _switch;}
    void setSwitch(bool b, bool autoWidth=true);
    bool isChecked() const {return isToggled();}

signals:
    void switched(bool);

protected:
    void paintEvent(QPaintEvent*) override;
    void mouseReleaseEvent(QMouseEvent*) override;
    void enterEvent(QEvent*) override;
    void resizeEvent(QResizeEvent *event) override;

private:
    bool _switch;
    qreal _opacity;
    int _x, _y, _height, _margin;
    QBrush _thumb, _track, _brush;
    QPropertyAnimation *_anim = nullptr;
};

#endif // SWITCHBUTTON_HH
