#include "../include/SwitchButton.hh"

SwitchButton::SwitchButton(QWidget *parent) : QAbstractButton(parent)
{
    _height = 16;
    _opacity = 0.000;
    _switch = false;
    _margin = 3;
    _thumb = QBrush("#d5d5d5");
    _anim = (new QPropertyAnimation(this, "offset", this));

    setOffset(_height / 2);
    _y = _height / 2;
    setBrush(QColor("#009688"));
}

SwitchButton::SwitchButton(const QBrush &brush, QWidget *parent) : QAbstractButton(parent)
{
    _height = 16;
    _opacity = 0.000;
    _switch = false;
    _margin = 3;
    _thumb = QBrush("#d5d5d5");
    _anim = (new QPropertyAnimation(this, "offset", this));

    setOffset(_height / 2);
    _y = _height / 2;
    setBrush(brush);
}

void SwitchButton::paintEvent(QPaintEvent */*e*/) {
    QPainter p(this);
    p.setPen(Qt::NoPen);
    if (isEnabled()) {
        p.setBrush(_switch ? brush() : Qt::black);
        p.setOpacity(_switch ? 0.5 : 0.38);
        p.setRenderHint(QPainter::Antialiasing, true);
        p.drawRoundedRect(QRect(_margin, _margin, width() - 2 * _margin, height() - 2 * _margin), 8.0, 8.0);
        p.setBrush(_thumb);
        p.setOpacity(1.0);
        p.drawEllipse(QRectF(offset() - (_height / 2), _y - (_height / 2), height(), height()));
    } else {
        p.setBrush(Qt::black);
        p.setOpacity(0.12);
        p.drawRoundedRect(QRect(_margin, _margin, width() - 2 * _margin, height() - 2 * _margin), 8.0, 8.0);
        p.setOpacity(1.0);
        p.setBrush(QColor("#BDBDBD"));
        p.drawEllipse(QRectF(offset() - (_height / 2), _y - (_height / 2), height(), height()));
    }
}

void SwitchButton::mouseReleaseEvent(QMouseEvent *e) {
    if (e->button() & Qt::LeftButton) {
        _switch = _switch ? false : true;
        _thumb = _switch ? _brush : QBrush("#d5d5d5");
        if (_switch) {
            _anim->setStartValue(_height / 2);
            _anim->setEndValue(width() - _height);
            _anim->setDuration(120);
            _anim->start();
        } else {
            _anim->setStartValue(offset());
            _anim->setEndValue(_height / 2);
            _anim->setDuration(120);
            _anim->start();
        }

        emit switched(_switch);
    }
    QAbstractButton::mouseReleaseEvent(e);
}

void SwitchButton::enterEvent(QEvent *e) {
    setCursor(Qt::PointingHandCursor);
    QAbstractButton::enterEvent(e);
}

void SwitchButton::resizeEvent(QResizeEvent *event)
{
    QAbstractButton::resizeEvent(event);

    if(_switch)
    {
        int endValue = width() - _height;

        setOffset(endValue);
    }
}

QSize SwitchButton::sizeHint() const {
    return QSize(2 * (_height + _margin), _height + 2 * _margin);
}

void SwitchButton::setSwitch(bool b, bool autoWidth)
{
    bool emitSignal = (b != _switch);

    _switch = b;
    _thumb = _switch ? _brush : QBrush("#d5d5d5");

    int endValue = offset();

    if(_switch)
    {
        int widthC;
        if(autoWidth)
            widthC = width();
        else
            widthC = 2 * (_height + _margin);

        endValue = widthC - _height;
    }
    else
        endValue =  _height / 2;

    setOffset(endValue);

    if(emitSignal)
        emit switched(b);
}
