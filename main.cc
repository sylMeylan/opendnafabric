#include <QApplication>
#include <QResource>
#include <QtWebEngine/QtWebEngine>

#include "SymVox/Run/Management/include/RunManager.hh"
#include "App/include/AppManager.hh"
#include "GUI/Management/include/MainWindow.hh"

/** @file */

/**
 * @brief Main function of the DnaFabric application
 * @param argc The number of arguments (including the name of the executable) given when starting the program.
 * @param argv The value of each argument.
 * @return Exit status
 *
 * This is the main function of DnaFabric. The different parts of Qt and SymVox libraries are created and initialized here.
 * There should be:
 *  - A QApplication instance followed by "QtWebEngine::initialize();" since the program uses QtWebEngine.
 *  - An AppManager with the name of the application.
 *  - A RunManager that should be given a pointer to the AppManager and then be initialized and started.
 */
int main(int argc, char** argv)
{
    // We automate the use of the SymVox namespace
    using namespace SV;

    // To use qt we need to create a qt app
    QApplication app(argc, argv);
    QtWebEngine::initialize();

    // Required to access SymVox resource from the application.
    // It includes the SymVox shaders for example.
    Q_INIT_RESOURCE(symvox_resource);

    // App manager
    AppManager appManager("OpenDnaFabric");

    // Create the run manager
    SV::Run::RunManager run;

    // Attach the application
    run.SetAppManager(&appManager);

    // Attach a custom main window
    run.SetMainWin(new MainWindow(appManager) );

    // Initialize
    run.Initialize();

    // Start
    run.StartMainLoop();

    QThreadPool::globalInstance()->waitForDone();

    // End of the program
    return 0;
}
