![Fig-0 IRSN Logo.](doc/logo_irsn.png)

OpenDnaFabric Presentation
=========

# Introduction {#section0}

DnaFabric is an application dedicated to the creation, modification, visualization and export of complex DNA geometries.
It was developed by the french Institut de Radioprotection et de Sûreté Nucléaire (IRSN, https://www.irsn.fr/) and SymAlgo Technologies (www.symalgo-tech.com).

The application written in C++ and based on the SymVox open-source framework and the Qt libraries.
Note that SymVox also depends on Qt and requires a working installation of the Qt library (>= 5.9) and zlib.
Currently, DnaFabric is compatible with linux and more precisely Ubuntu 16-04 LTS. It should be possible to compile and install
the software on MacOS and Windows but it will require additional work to extend the
CMakeFiles currently used to process DnaFabric source files.

A new user looking at the DnaFabric source code should start by reading this documentation before jumping in the code of the project.
Once the documentation is read, we advise to start with the main.cc file since it is where
all Qt and SymVox main blocks are defined and activated: AppManager, RunManager etc.
Then, try to look at the CellSheetApp class (App/include/CellSheetApp.hh) which is the "pilot" class of the CellSheet application
included within DnaFabric. At the moment, this application is the one that is "production ready" within the software. Others exist but
are only used for test purposes.
In the end, the CellSheetApp class can be considered as the "second main file" of DnaFabric.
Specifically, the Load() and LoadUI() methods drive the load and unload processes of the application and the
Process() method defines what it does when the application is "updated".
The applicaton is continuously "updated" when running to allow the 3D rendering in real time and
the processing of user actions.

This documentation is especially useful to learn:
1. How to compile and setup DnaFabric and SymVox from their sources (see section \ref section1-0 and \ref section1-1). 
2. How to generate the documentation of DnaFabric (see sections \ref section \ref section1-2).
3. The general structure of the source code (see section \ref section2).

To download the source code, you can use git (see the clone button) or download it directly as a zip archive (https://bitbucket.org/sylMeylan/opendnafabric/downloads/).

# Setup {#section1}

DnaFabric is an application using the SymVox and Qt libraries as base building blocks. 
As such, the installation of both libraries is mandatory for DnaFabric to compile and execute on the user system.

SymVox requires that your system has an OpenGL version (Core version) superior or equal to 4.0. You can check your version with the following console command:
```{.sh}
glxinfo | grep 'version'
// In the output, look at the line with "Max Core profile version...".
// This line contains your OpenGL Core profile version.
```

OpenDnaFabric requires a version of Qt superior or equal to 5.9. To install it, you can download the qt installator from there:
https://www.qt.io/download
Execute the installator to install Qt5.9. Then, you will need to edit your .profil file for cmake to find the qt you just downloaded.
 ```{.sh}
// 1) Open your ~/.profil file with a text editor
// 2) At the end of the file, add the following: 
export CMAKE_PREFIX_PATH={YourPathToQt5.9}/{5.9}/gcc_64 // Adjust the path to match your configuration
LD_LIBRARY_PATH={YourPathToQt5.9}/5.9/gcc_64/lib // Adjust the path to match your configuration
// 3) Close and restart your terminal for the modification to be taken into account.
```

## SymVox compilation and setup {#section1-0}

Get the source code of the project:

```{.sh}
git clone https://sylMeylan@bitbucket.org/sylMeylan/symvox.git
```

Create a folder named symvox-build-res to compile the project in "REALEASE" mod. 

```{.sh}
mkdir symvox-build-rel
cd symvox-build-rel
```

Configure the projet with CMAKE:

```{.sh}
cmake -DSYMVOX_USE_AS_LIB=ON ../symvox # Relative path to the source code folder
```

Then, start the compile process:

```{.sh}
make
```

And trigger the installation of the library

```{.sh}
sudo make install
```

## DnaFabric compilation and setup {#section1-1}

Create a folder to store the source code of the project:
```{.sh}
mkdir dnafabric
```
Paste the code in the folder. 

Create a folder named symvox-build-res to compile the project in "REALEASE" mod. 
```{.sh}
mkdir dnafabric-build-rel
cd dnafabric-build-rel
```

Configure the projet with CMAKE:
```{.sh}
cmake ../dnafabric # Relative path to the source code folder
```

Then, start the compile process:
```{.sh}
make
```

DnaFabric is now compiled and an executable is located in the compilation folder ("dnafabric-build-rel") we just created. 

## Documentation creation {#section1-2}

OpenDnaFabric source code is documented with Doxygen format and the documentation can be generated with:
```{.sh}
 # Go into the source code folder
cd DnaFabric;

 # Create a "doc" folder to save the documentation.
mkdir ../doc;
 
 # Open Doxywizard
doxywizard ./DoxyFile;
```
Then, go in the run tab and click on "run" to generate the documentation (html and latex format).

The "html" and "latex" folders are then created inside the "doc" folder. HTml format can be displayed by a any web-browser and latex format allow the creation of a PDF file. 
In order to generate a PDF file, a user should open the latex/refman.tex file with a latex editor (such as TexMaker) and compile it. This compilation may require the installation of additionnel laex packages depending on the user system. 
Error messages will explicitely ask for those if needed. 

Please, note that correct display of figure labels imply the manual edition of the refman.tex file before compilation:

```{.latex}
 % Replace the content of line 113 by:
\captionsetup{labelformat=empty,labelsep=space,justification=centering,font={bf},singlelinecheck=off,skip=4pt,position=top}
```

# Code structure {#section2}

## Introduction {#section2-1}

DnaFabric source code is organized in four main parts plus two "utility" parts, one main.cc file and one CMakeLists.txt file.
Their functions are detailed below.
- **CMakeLists.txt file** DnaFabric uses CMake to organize and compile its source code. Thus, the CMakeLists.txt file located at the root
of the
project defines the "project" for a development environment (QtCreator) as well as the build processes and parameters.
Each subfolder of DnaFabric source code also contains a CMakeLists.txt file listing all the local files that should be included in the
project at configuration time.
- **cmake folder** Contains several utility methods used within the main CMakeLists files of the project. These allow the recursive
addition of files as well as the differenciation between pure C++, resources and Qt-C++ files.
- **main.cc file** This is the entry point of the application and where the different libraries used within DnaFabric are initialized and started.
- **App folder** Contains the CellSheetApp class that is the second main file of DnaFabric.
- **Logic folder** Contains the objects and alorigthms used to build the DnaFabric backend.
- **GUI folder** Contains the objects and resources required to define the DnaFabric GUI. It uses both QtWidget and Qml.
- **Controller folder** Contains the controller classes in charge of linking the GUI with the Logic of the application.
- **Data folder** Stores the static resource files used within DnaFabric (images and icons).

Besides DnaFabric files and folders, there are also a **doc folder** containing the images used in this documentation and a **DoxyFile file** used
to save the parameters of the present documentation. You may also found a **git folder** to store the code modification history
and several **qmodel files** that illustrate some of the DnaFabric processes.

This section will introduce the general run-flow of DnaFabic, the structure of the CellSheet application will be explained and the
simulation algorithms present within the code will be described.

## DnaFabric application run flow {#section2-2}

Having a good general understanding of the DnaFabric run-flow is essential to see the global picture behind the details of the code.
The schema below was made to provide a tool to ease the process of forming this global picture for a new user.
As such, the schema describes the global run-flow of DnaFabric: what is done and in which order after
a user starts the program.
At first glance, we can see there are two group-boxes on the schema to illustrate the steps performed by DnaFabric alone and the ones performed
by SymVox.

When a user starts DnaFabric, the program executes the instructions contained in the main.cc file. These instructions initialize Qt and SymVox and
start the SymVox RunManager. Then, this RunManager starts the initialization method of the UserApp (which is the CellSheetApp in our case).
The UserApp initialization is done through a call to the VUserApp::Load() function and it is where the different systems used by DnaFabric are instanciated
and their parameters set. These systems can be seen
as modules defined within SymVox or within the code of DnaFabric itself. For example, there is a module to render the 3D view, another to
manage the Camera moving in the 3D world, another to deal with the simulation mecanism (sucession of steps and operations).
The VUserApp::LoadUI() function is called just after VUserApp::Load() to instanciate a set of DnaFabric classes named controllers. Controllers
are the bridge between two parts of DnaFabric : the application logic (Logic) and the interface (GUI).
Once the initialization is done, the program enters its so-called "main loop" (see the yelow losange in \ref Fig-1). This "main loop" is
a piece of code called 30 times per seconds (this value may be adjusted by the user) and located within SymVox. As such, you will not see it
within DnaFabric source code.
The main loop performs several tasks such as checking if the program should be interrupted, perform some computations, process a QtEvent
thrown by DnaFabric. The main loop especially calls VUserApp::Process() function that is defined ("overloaded" precisely) within the CellSheetApp.
In the end, most of the work is done in this CellSheetApp::Process() function: 3D rendering, simulations, user actions etc.

As previously stated, each main-loop iteration calls the VUserApp::Process() function that is implemented in the CellSheetApp class.
This function calls in turn the Process() function of each system previously instanciated within the VUserApp::Load(). This is where the systems
and controllers do their work and update themselves (or the interface) according to user input or simulation result. Since the update
(or refresh rate) is really fast (about 30 times per seconds) it gives the illusion of real-time from a 3D rendering point of view.

Finally, if the user choose to exit the program then the main-loop will acknowledge it and interrupt itself. The interruption will
be followed by a cleaning process that will uninitialize and remove everything related to DnaFabric by calling the VUserApp::Unload and
VUserApp::UnloadUI functions. Then, the objects managed by SymVox will be deleted and the program will safely exit.

![Fig-1 Schema of the DnaFabric run flow.](doc/img/dnafabric_runtime.png)
