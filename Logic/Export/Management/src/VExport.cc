#include "../include/VExport.hh"

namespace Export {

VExport::VExport() :
    fpRootNode(nullptr),
    fFileName("NoName")
{

}

VExport::VExport(SV::Run::VSceneNode* rootNode) :
    fpRootNode(rootNode),
    fFileName("NoName")
{

}

VExport::~VExport()
{

}

bool VExport::Export(const std::string& fileName)
{
    if(fpRootNode==nullptr)
    {
        std::cerr<<"**** Warning ****"<<std::endl;
        std::cerr<<"ExportToG4DNA::Export: The world was not set."<<std::endl;
        std::cerr<<"Nothing will be done."<<std::endl;
        std::cerr<<"****************"<<std::endl;

        return false;
    }

    fFileName = fileName;

    std::fstream file;
    file.open(fileName, std::ios_base::out);

    WriteFileHeader(file);

    // Recursively analyse the world in order to detect and export all the appropriate geometrical objects
    RecursivelyAnalyseNode(fpRootNode, file);

    file.close();

    return true;
}

void VExport::RecursivelyAnalyseNode(SV::Run::VSceneNode* node, std::fstream& file)
{
    bool processChilds = ProcessNode(node, file);

    if(processChilds)
    {
        // Loop on all the childs
        for(int i=0, ie=node->GetNumOfChildren(); i<ie; i++)
        {
            // Get the current child
            SV::Run::VSceneNode* child = node->GetChild(i);

            RecursivelyAnalyseNode(child, file);
        }
    }
}

}

