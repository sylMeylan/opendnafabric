#ifndef EXPORT_VEXPORT_HH
#define EXPORT_VEXPORT_HH

#include <fstream>

#include "SymVox/Run/Scene/Graph/include/VSceneNode.hh"

namespace Export {

class VExport
{
public:
    VExport();
    VExport(SV::Run::VSceneNode* rootNode);

    virtual ~VExport();

    virtual bool Export(const std::string& fileName);

    void SetRootNode(SV::Run::VSceneNode* world) {fpRootNode=world;}

    SV::Run::VSceneNode* GetRootNode() {return fpRootNode;}

    const SV::Run::VSceneNode* GetRootNode() const {return fpRootNode;}

protected:

    SV::Run::VSceneNode* fpRootNode;
    std::string fFileName;

    virtual void WriteFileHeader(std::fstream& file) = 0;

    virtual void RecursivelyAnalyseNode(SV::Run::VSceneNode* node, std::fstream& file);
    virtual bool ProcessNode(SV::Run::VSceneNode* node, std::fstream& file) = 0;
};

}

#endif // EXPORT_VEXPORT_HH
