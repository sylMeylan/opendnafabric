#include "../include/DocExport.hh"

DocExport::DocExport() : QObject(nullptr)
{
    fExportFilePath = "";

    fpView = new QWebEngineView();
    fpWebEnginePage = new QWebEnginePage(QWebEngineProfile::defaultProfile(), fpView);

    QObject::connect(fpWebEnginePage, &QWebEnginePage::loadFinished,
                     this, [&]() {
        QPageLayout layout( QPageSize( QPageSize::A4 ), QPageLayout::Portrait, QMarginsF(5, 5, 5, 5) );
        fpWebEnginePage->printToPdf(fExportFilePath, layout);
    });

    QObject::connect(fpWebEnginePage, &QWebEnginePage::pdfPrintingFinished,
                     this, [&](const QString &/*filePath*/, bool /*success*/) {
        emit finished();
    });
}

DocExport::~DocExport()
{
    delete fpView;
}

void DocExport::ExportHtmlToDoc(const QString &exportFilePath, const QString &html)
{
    fpWebEnginePage->setHtml(html); // Asynchronous method
    fExportFilePath = exportFilePath;

    // The slots defined in the constructor will be called then
}

