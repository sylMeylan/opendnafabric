#ifndef DOCEXPORT_HH
#define DOCEXPORT_HH

#include <QWebEnginePage>
#include <QWebEngineView>
#include <QObject>
#include <QWebEngineProfile>

/**
 * @brief The DocExport class implements fontionnalities to export data to a pdf file
 */
class DocExport : public QObject
{
    Q_OBJECT

public:
    DocExport();
    ~DocExport();

    /**
     * @brief ExportHtmlToDoc allows to create and fill a pdf file.
     * @param exportFilePath The path to the pdf file to be created
     * @param html The content of the pdf file encoded with html.
     */
    void ExportHtmlToDoc(const QString& exportFilePath, const QString& html);

signals:
    void finished();

private:
    QWebEngineView *fpView;
    QWebEnginePage* fpWebEnginePage;
    QString fExportFilePath;

};

#endif // DOCEXPORT_HH
