#ifndef EXPORT_EXPORTVOXEL_HH
#define EXPORT_EXPORTVOXEL_HH

#include "Logic/Export/Management/include/VExport.hh"

#include "Logic/BioStructures/Cell/DNA/Voxels/include/VVoxel.hh"

namespace Export { namespace Geant4 {

class ExportVoxel : public VExport
{
public:
    // C++11 constructor forwarding
    using VExport::VExport;

    ~ExportVoxel();

protected:

    virtual void WriteFileHeader(std::fstream& file);

    virtual bool ProcessNode(SV::Run::VSceneNode* node, std::fstream& file) override;

    std::string DescribeNucleosome(Models::DNA::Molecular::Nucleosome& nucleosome, const QMatrix4x4& frame);
    std::string DescribeLinker(Models::DNA::Molecular::Linker& linker, const QMatrix4x4& frame);
    std::string DescribeNucleotide(Models::DNA::Molecular::NucleotidePair& nucleotidePair, const QMatrix4x4& frame);
    std::string DescribeMolecule(Models::DNA::Molecular::Molecule& molecule, const QMatrix4x4& frame);
};

}}

#endif // EXPORT_EXPORTVOXEL_HH
