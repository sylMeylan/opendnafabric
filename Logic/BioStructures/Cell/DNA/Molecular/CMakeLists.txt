#add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/BioStructures")

add_sources(
   Molecule.cc
   NucleotidePair.cc
   Nucleosome.cc
   Linker.cc
)

add_headers(
   Molecule.hh
   NucleotidePair.hh
   Nucleosome.hh
   Linker.hh
)

add_uis()
