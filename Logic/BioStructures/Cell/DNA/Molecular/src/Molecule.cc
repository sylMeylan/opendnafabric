#include "../include/Molecule.hh"

namespace Models { namespace DNA { namespace Molecular {

Molecule::Molecule()
    : SceneNodeCRTP(),
    fRadius(0),
    fWaterRadius(0),
    fStrand(0),
    fMaterial("noMaterial")
{
    SetType("Molecule");
}

Molecule::Molecule(const std::string& name)
    : SceneNodeCRTP(name),
    fRadius(0),
    fWaterRadius(0),
    fStrand(0),
    fMaterial("noMaterial")
{
    SetType("Molecule");
}

Molecule::Molecule(const std::string& name, double radius)
    : SceneNodeCRTP(name),
      fRadius(radius),
      fWaterRadius(0),
      fStrand(0),
      fMaterial("noMaterial")
{
    SetType("Molecule");
}

Molecule::Molecule(const std::string& name, double radius, QMatrix4x4 &posAndRot)
    : SceneNodeCRTP(name),
      fRadius(radius),
      fWaterRadius(0),
      fStrand(0),
      fMaterial("noMaterial")
{
    SetType("Molecule");
    SetModelMat(posAndRot);
}

Molecule::~Molecule()
{

}

Molecule::Molecule(const Molecule &toBeCopied)
    : SceneNodeCRTP(toBeCopied)
{
    // Just do here the basic copies.
    // Those are done by default by the auto generated copy constructor.
    // Here we overrided the auto generated to implement our own.
    // No particular interest here.

    fRadius = toBeCopied.fRadius;
    fWaterRadius = toBeCopied.fWaterRadius;
    fStrand = toBeCopied.fStrand;
    fMaterial = toBeCopied.fMaterial;

    // Here we need to perfom all the needed deep copies.

    // ...
}

Molecule &Molecule::operator=(Molecule rhs)
{
    SceneNodeCRTP::operator=(rhs);

    std::swap(fRadius,rhs.fRadius);
    std::swap(fWaterRadius,rhs.fWaterRadius);
    std::swap(fStrand,rhs.fStrand);
    std::swap(fMaterial, rhs.fMaterial);

    return *this;
}

}}}
