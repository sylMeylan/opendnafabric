#include "../include/Nucleosome.hh"

namespace Models { namespace DNA { namespace Molecular {

Nucleosome::Nucleosome(int nucleosomeCopyNumber, int firstElementCopyNumber)
    : SV::Run::SceneNodeCRTP<SV::Run::VSceneNode, Nucleosome>(),
      fInitialElementNumber(154),
      fFirstElementCopyNum(firstElementCopyNumber),
      fIsGenerated(false),
      fIsNuclAdjustable(true),
      fHistoneIndex(-1)
{
    SetId(nucleosomeCopyNumber);

    Molecule histone("histone");
    histone.SetRadius(2.4);
    histone.SetId(GetId() );
    AddHistone(histone);

    SetType("Nucleosome");
    SetName("Nucleosome");

    DefineLods();
}

Nucleosome::~Nucleosome()
{

}

Nucleosome::Nucleosome(const Nucleosome &toBeCopied)
    : SceneNodeCRTP(toBeCopied)
{
    fInitialElementNumber = toBeCopied.fInitialElementNumber;
    fFirstElementCopyNum = toBeCopied.fFirstElementCopyNum;
    fIsGenerated = toBeCopied.fIsGenerated;
    fIsNuclAdjustable = toBeCopied.fIsNuclAdjustable;
    fHistoneIndex = toBeCopied.fHistoneIndex;
    fSizeIndiator = toBeCopied. fSizeIndiator;
    fDNAElementIndex = toBeCopied.fDNAElementIndex;
}

NucleotidePair &Nucleosome::GetFirstDNAElement()
{
    return GetDNAElement(0);
}

NucleotidePair &Nucleosome::GetLastDNAElement()
{
    return GetDNAElement(GetElementNumber()-1 );
}

NucleotidePair &Nucleosome::GetDNAElement(int index)
{
    return *dynamic_cast<NucleotidePair*>(GetChild(fDNAElementIndex[index]) );
}

const NucleotidePair &Nucleosome::GetDNAElement(int index) const
{
    return *dynamic_cast<const NucleotidePair*>(GetChild(fDNAElementIndex[index]) );
}

Molecule &Nucleosome::GetHistone()
{
    return *dynamic_cast<Molecule*>(GetChild(fHistoneIndex) );
}

const Molecule &Nucleosome::GetHistone() const
{
    return *dynamic_cast<const Molecule*>(GetChild(fHistoneIndex) );
}

void Nucleosome::SetFirstElementCopyNum(int firstElementCopyNum)
{
    fFirstElementCopyNum=firstElementCopyNum;

    // Loop on all the DNA elements
    for(unsigned int element=0, elementEnd=fDNAElementIndex.size(); element<elementEnd; ++element)
    {
        GetDNAElement(element).SetCopyNumber(fFirstElementCopyNum+element);
    }
}

void Nucleosome::AddDNAElementFromLast(int num)
{
    // Loop on all the DNA element to be removed starting from the end of the nucleosome
    for(int i=0; i<num; ++i)
    {
        // Get the a copy of the last dna element of the nucleosome
        NucleotidePair element = GetDNAElement(GetElementNumber()-1 );

        element.RotateInFrame(360./77, QVector3D(0, 0, 1 ), GetModelMat() );
        element.TranslateInLocalFrame(QVector3D(0, 0, element.GetDistance() ) );
        element.RotateInLocalFrame(element.GetPivotAngle(), QVector3D(0,0,1) );

        AddDNAElement(element);
    }
}

void Nucleosome::RemoveDNAElementFromLast(int num)
{
    // Loop on all the DNA element to be removed starting from the end of the nucleosome
    for(int i=0; i<num; ++i)
    {
        // Remove the object
        RemoveDNAElement(GetElementNumber()-1 );
    }
}

void Nucleosome::ApplyPivotOffset(float angle)
{
    // Loop on all the DNA elements
    for(unsigned int element=0, elementEnd=fDNAElementIndex.size(); element<elementEnd; ++element)
    {
        GetDNAElement(element).RotateInLocalFrame(angle, QVector3D(0,0,1) );
    }
}

void Nucleosome::GenerateNucleosome()
{
    // A nucleosome is made of a central histone surrounded by a twisted double helix DNA.

    // First, get the nucleosome copy number.
    int nucleosomeCpNum = this->GetId();

    // Assign it to the histone.
    GetHistone().SetId(nucleosomeCpNum);

    // Erase everything within the DNA element container.
    // Clean start.
    RemoveDNAElementFromLast(fDNAElementIndex.size() );

    // Setup the size of the container vector
    //fDNAElementIndex.resize(fInitialElementNumber);
    fDNAElementIndex.clear();

    QMatrix4x4 elementPosMatrix;
    elementPosMatrix.setToIdentity();

    // Element to be used as base to place all the others.
    // Others will be copied within the vector container.
    NucleotidePair elementDummy;

    // Translation to go at the first base point, the (0,0,0) point being the histone center
    elementDummy.TranslateInLocalFrame(QVector3D(4.045, 0, -2.370) );

    // We rotate the element of -90 deg along the X axis to orientate its Z axis towards the direction the DNA will follow.
    elementDummy.RotateInLocalFrame(-90, QVector3D(1,0,0) );

    // Rotate along X to orientate the Y axis a little bit towards the screen deeper along the Z axis.
    //
    //
    // Try to picture the above schema as a rectangle triangle...
    //
    // Hypothenus=d=0.331412
    //                                                                         ___/ |
    //                                                              ______/       |
    //                                             _________/                   |
    //                                ________/                                  |  b=secondHelixPitch/nbBasePairPerTurn=2.370/77
    //         ____________/                                                _|
    // ___/ sin(alpha)=b/d                                90 deg | |
    // ----------------------------------------------------------------------
    //
    // alpha = asin(b/d) = 5.33
    //
    // Alpha is the rotation angle which orientate the dna towards "in" the screen.
    //
    float alpha = 5.33;
    elementDummy.RotateInLocalFrame(alpha, QVector3D(1,0,0) );

    // By doing so, we change the orientation of the Y axis.
    // We need to keep the original Y axis to use it as rotation axis in order to twist the double helix aroung the histone.
    //
    QGenericMatrix<3,1, float> yAxis; // 3 columns and 1 row (N,M)
    //QGenericMatrix<1,3, float> yAxis; // 1 column and 3 rows (N,M)
    yAxis(0,0) = 0;
    yAxis(0,1) = 1;//yAxis(1,0) = 1;
    yAxis(0,2) = 0;//yAxis(2,0) = 0;
    QMatrix4x4 rotationMatrix;
    rotationMatrix.setToIdentity();
    rotationMatrix.rotate(+alpha, QVector3D(1,0,0) );
    QMatrix3x3 rotationMat = ExtractRotMatFromPosAndRot(rotationMatrix);
    yAxis = yAxis * rotationMat;

    // Loop on all the DNA elements
    for(int i=0, ie=fInitialElementNumber; i<ie; ++i)
    {
        // Set the copy number starting from the first copy number specified in the constructor
        elementDummy.SetCopyNumber(fFirstElementCopyNum+i);

        // Change the name of the first nucleotide pair to avoid the
        // triggering of multiple warnings.
        // They come from the fact that two elements with the same name
        // are not loaded as distinct 3D objects.
        // The name make the distinction here.
        if(i==1)
        {
            std::stringstream ss;
            ss << elementDummy.GetName() << "_" << i;
            elementDummy.SetName(ss.str() );
        }

        // Add the element within the child container
        AddDNAElement(elementDummy);

        // Make the element rotate to build the double helix shape.
        // We do it now and in the container (not the dummyElement) to avoid mixing up the axis for the next iteration.
        //float angle = i*elementDummy.GetPivotAngle() %360; // bitwise operations are not working with float or double
        float angle = std::fmod(i*elementDummy.GetPivotAngle(), 360);

        GetDNAElement(i).RotateInLocalFrame(angle, QVector3D(0,0,1) );

        // Compute the next position.
        // To do so, we update elementPosMatrix.

        // If we translate in the Y direction (picture a screen and Y is its height)
        // then we will go up and towards the screen.
        // But we also want to get to the right at the same time to turn around the histone protein.
        // This is to go to the right.
        //
        // We rotate along Z, which is Y in the frame of the element.
        // 360 deg: 1 turn
        // 77: number of base pairs per turn
        //
        //elementDummy.RotateInLocalFrame(-360./77, QVector3D(yAxis(0,0), yAxis(1,0), yAxis(2,0) ) );
        elementDummy.RotateInLocalFrame(-360./77, QVector3D(yAxis(0,0), yAxis(0,1), yAxis(0,2) ) );

        // Translate along the Y axis, which is Z in the frame of the element.
        elementDummy.TranslateInLocalFrame(QVector3D(0, 0, elementDummy.GetDistance() ) );
    }

    // Set the flag to true once the nucleosome is generated.
    fIsGenerated = true;
}

void Nucleosome::AddDNAElement(NucleotidePair &element)
{
    NucleotidePair* el = new NucleotidePair(element);

    AddChild(el);

    fDNAElementIndex.push_back(GetNumOfChildren()-1);
}

void Nucleosome::AddHistone(Molecule &histone)
{
    if(fHistoneIndex==-1)
    {
        Molecule* hist = new Molecule(histone);

        AddChild(hist);

        fHistoneIndex = GetNumOfChildren()-1;
    }
    else
    {
        std::ostringstream oss;
        oss << "A histone is already here.";
        SV::Utils::Exception(SV::Utils::ExceptionType::Fatal, "Nucleosome::AddHistone", oss.str() );
    }
}

void Nucleosome::RemoveDNAElement(int index)
{
    RemoveChild(fDNAElementIndex[index]);

    std::vector<int> newIndex;

    // Loop on all the old index
    for(int i=0, ie=fDNAElementIndex.size(); i<ie; ++i)
    {
        // Do something only if we are not at the current index that we want to remove
        if(i != index)
        {
            newIndex.push_back(fDNAElementIndex[i]);
        }
    }

    fDNAElementIndex.clear();
    fDNAElementIndex.resize(newIndex.size() );
    fDNAElementIndex = newIndex;
}

void Nucleosome::RemoveHistone()
{
    if(fHistoneIndex>=0)
    {
        RemoveChild(fHistoneIndex);

        fHistoneIndex = -1;
    }
    else
    {
        std::ostringstream oss;
        oss << "There is no histone to be removed.";
        SV::Utils::Exception(SV::Utils::ExceptionType::Fatal, "Nucleosome::RemoveHistone", oss.str() );
    }
}

void Nucleosome::DefineLods()
{
    // ***********************
    // Histone LODs
    // ***********************

    SV::Logic::Renderable* histoneRenderable = new SV::Logic::Renderable;
    GetHistone().AddComponent(histoneRenderable, SV::Logic::List::Renderable);

    SV::Logic::LodModel histoneLod;
    AutoSetLodForMol(GetHistone(), 16, 8, 100, histoneLod); histoneRenderable->AddLod(histoneLod);
    AutoSetLodForMol(GetHistone(), 8, 4, 800, histoneLod); histoneRenderable->AddLod(histoneLod);
    AutoSetLodForMol(GetHistone(), 8, 4, 2000, histoneLod); histoneRenderable->AddLod(histoneLod);

    // Nothing

    histoneLod.Set3dType("Histone_3");
    histoneLod.SetShowChildren(false);
    histoneLod.SetDistance(3000);
    histoneLod.SetPrimitive(nullptr);
    histoneRenderable->AddLod(histoneLod);

    // ************************
    // Nucleosome LODs
    // ************************

    SV::Logic::Renderable* nucleosoRenderable = new SV::Logic::Renderable;
    AddComponent(nucleosoRenderable, SV::Logic::List::Renderable);

    SV::Logic::LodModel nucleosoLod;
    nucleosoLod.Set3dType("Nucleosome_0");
    nucleosoLod.SetDistance(500);
    nucleosoLod.SetPrimitive(new SV::TriD::CompoObjPrimitive);
    nucleosoLod.SetChildForcedIndex(0);
    nucleosoLod.SetIsMultiMesh(false);
    nucleosoLod.SetShowChildren(true);
    nucleosoLod.SetColorInstancing(false);
    nucleosoLod.SetSizeIndicator(GetHistone().GetRadius()*4);
    nucleosoLod.SetShaders(":/SymVox/3D/Shaders/multi_lights_instancing.vert",
                                ":/SymVox/3D/Shaders/multi_lights_instancing.frag");
    nucleosoRenderable->AddLod(nucleosoLod);

    nucleosoLod.Set3dType("Nucleosome_1");
    nucleosoLod.SetDistance(1000);
    nucleosoLod.SetChildForcedIndex(1);
    nucleosoLod.SetIsMultiMesh(false);
    nucleosoRenderable->AddLod(nucleosoLod);

    nucleosoLod.Set3dType("Nucleosome_2");
    nucleosoLod.SetDistance(1500);
    nucleosoLod.SetChildForcedIndex(2);
    nucleosoLod.SetIsMultiMesh(false);
    nucleosoRenderable->AddLod(nucleosoLod);

    float size = 3*GetHistone().GetRadius();
    nucleosoLod.Set3dType("Nucleosome_3");
    nucleosoLod.SetDistance(2000);
    nucleosoLod.SetColorInstancing(true);
    nucleosoLod.SetIsMultiMesh(false);
    nucleosoLod.SetPrimitive(new SV::TriD::Cube(size) );
    nucleosoLod.SetColor(QColor(0, 200, 0) );
    nucleosoLod.SetChildForcedIndex(3);
    nucleosoRenderable->AddLod(nucleosoLod);
}

void Nucleosome::AutoSetLodForMol(Molecule &m, int u, int v, float distance, SV::Logic::LodModel &lod, bool visible)
{
    SV::TriD::Sphere* sphere = nullptr;
    SV::Logic::Renderable* renderable = m.GetRenderable();

    if(visible)
        sphere = new SV::TriD::Sphere(QVector3D(), m.GetRadius(), u, v);

    std::ostringstream oss;
    // Phosphate_0, Phosphate_1 etc.
    oss << m.GetName() << "_" << renderable->GetNumOfLods();
    std::string name = oss.str();

    lod.Set3dType(name);
    lod.SetPrimitive(sphere);
    lod.SetSizeIndicator(m.GetRadius()*2);
    lod.SetColor(QColor(255,0,0) ); // red
    lod.SetColorInstancing(true);
    lod.SetDistance(distance);
    lod.SetShowChildren(true);
    lod.SetShaders(":/SymVox/3D/Shaders/multi_lights_instancing_color.vert",
                   ":/SymVox/3D/Shaders/multi_lights_instancing_color.frag");
}

}}}
