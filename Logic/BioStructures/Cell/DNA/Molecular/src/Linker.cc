#include "../include/Linker.hh"

namespace Models { namespace DNA { namespace Molecular {

Linker::Linker() : SV::Run::SceneNodeCRTP<SV::Run::VSceneNode, Linker>(),
    fIsGenerated(false),
    fFirstElemCopyNum(0),
    fElemNumber(10),
    fBezierCurveNbP(1000),
    fMaxElemNumber(0),
    fpStartBasicElement(0),
    fpEndBasicElement(0)
{
    SetName("Linker");
    SetType("Linker");

    fControlPointDistance1 = 6.;
    fControlPointDistance2 = 6.;
}

Linker::Linker(NucleotidePair &startElement, NucleotidePair &endElement, SV::Run::VSceneNode *parentNode)
    : SV::Run::SceneNodeCRTP<SV::Run::VSceneNode, Linker>(),
      fIsGenerated(false),
      fFirstElemCopyNum(0),
      fElemNumber(300),
      fBezierCurveNbP(1000),
      fMaxElemNumber(0),
      fpStartBasicElement(&startElement),
      fpEndBasicElement(&endElement),
      fpVoxel(parentNode)
{
    SetName("Linker");
    SetType("Linker");

    fControlPointDistance1 = 6.;
    fControlPointDistance2 = 6.;
    fSizeIndicator = 10;

    SetModelMat(fpVoxel->GetModelMat() );
}

Linker::~Linker()
{

}

Linker::Linker(const Linker &toBeCopied) : SV::Run::SceneNodeCRTP<SV::Run::VSceneNode, Linker>(toBeCopied)
{
    fIsGenerated = toBeCopied.fIsGenerated;
    fFirstElemCopyNum = toBeCopied.fFirstElemCopyNum;
    fElemNumber = toBeCopied.fElemNumber;
    fBezierCurveNbP = toBeCopied.fBezierCurveNbP;
    fMaxElemNumber = toBeCopied.fMaxElemNumber;
    fControlPointDistance1 = toBeCopied.fControlPointDistance1;
    fControlPointDistance2 = toBeCopied.fControlPointDistance2;
    fpStartBasicElement = toBeCopied.fpStartBasicElement;
    fpEndBasicElement = toBeCopied.fpEndBasicElement;
    fpVoxel = toBeCopied.fpVoxel;
    fDNAElementIndex = toBeCopied.fDNAElementIndex;
    fSizeIndicator = toBeCopied.fSizeIndicator;
}

Linker &Linker::operator=(Linker toBeCopied)
{
    SceneNodeCRTP::operator=(toBeCopied);

    std::swap(fIsGenerated ,toBeCopied.fIsGenerated);
    std::swap(fFirstElemCopyNum ,toBeCopied.fFirstElemCopyNum);
    std::swap(fElemNumber ,toBeCopied.fElemNumber);
    std::swap(fBezierCurveNbP , toBeCopied.fBezierCurveNbP);
    std::swap(fMaxElemNumber , toBeCopied.fMaxElemNumber);
    std::swap(fControlPointDistance1 , toBeCopied.fControlPointDistance1);
    std::swap(fControlPointDistance2 , toBeCopied.fControlPointDistance2);
    std::swap(fpStartBasicElement, toBeCopied.fpStartBasicElement);
    std::swap( fpEndBasicElement , toBeCopied.fpEndBasicElement);
    std::swap(fpVoxel , toBeCopied.fpVoxel);
    std::swap(fDNAElementIndex , toBeCopied.fDNAElementIndex);
    std::swap(fSizeIndicator, toBeCopied.fSizeIndicator);

    return *this;
}

void Linker::Generate()
{
    if(!fIsGenerated)
    {
        // If any...
        ClearDNAElements();

        if(fpStartBasicElement==0 || fpEndBasicElement==0)
        {
            std::cerr<<"***************** Fatal Error *****************"<<std::endl;
            std::cerr<<"DNALinker::Generate: Start DNA element and end DNA element were not defined for the linker."<<std::endl;
            std::cerr<<"*********************************************"<<std::endl;
            exit(EXIT_FAILURE);
        }

        // Get the start and end positions
        //
        QVector3D startPos = fpStartBasicElement->GetPosVectorInParentFrame(fpVoxel ); //->GetPosVectorInFrame(GetModelMat() );
        QVector3D endPos = fpEndBasicElement->GetPosVectorInParentFrame(fpVoxel ); //->GetPosVectorInFrame(GetModelMat() );

        // Start and end pos give us the p0 and p3 points.
        //
        Point p0(startPos.x(), startPos.y(), startPos.z() );
        Point p3(endPos.x(), endPos.y(), endPos.z() );

        // But we still need to determine p1 and p2 in order to generate the bezier curve.
        // The idea is to follow the direction of the first and last element on a fixed
        // distance (positive for first and negative for last element) to get
        // p1 and p2.

        // Ongoing direction is reprented by the z axis for any dna basic element.
        // Therefore, we need to determine what is the z axis of the first and last elements.

        // Standard z axis
        QVector3D zAxis(0,0,1);

        // Convert it to the current linker frame from the frames of the first and last elements.
        QVector3D firstZAxis = ConvertToLocalFromFrame(zAxis, fpStartBasicElement->GetParent()->GetModelMat()
                                                       * fpStartBasicElement->GetModelMat() );
        QVector3D endZAxis = ConvertToLocalFromFrame(zAxis,
                                                     fpEndBasicElement->GetParent()->GetModelMat()
                                                     * fpEndBasicElement->GetModelMat()
                                                     );
                //ConvertToLocalFromFrame(zAxis,fpEndBasicElement->GetModelMat());

        // Get p1 by adding fixed distance to the z axis
        QVector3D p1Pos = startPos + firstZAxis*fControlPointDistance1 + fControlPointOptionalVect1;
        Point p1(p1Pos.x(), p1Pos.y(), p1Pos.z() );

        // Get p2 by subtracting fixed distance to the z axis
        QVector3D p2Pos = endPos - endZAxis*fControlPointDistance2 + fControlPointOptionalVect2;
        Point p2(p2Pos.x(), p2Pos.y(), p2Pos.z() );

        // Now that we have the four points we can use bezier curve equation
        std::vector<Point> points = BezierCurve(p0, p1, p2, p3, fBezierCurveNbP);

        // Ok, we now have a lot of points coming from the bezier curve but we still need to pick some of them
        // in order to place our dna elements. The distance between each dna element being fixed (and this is
        // incompatible with a bezier curve by definition), we have to iterate and find the best point for each step.

        std::vector<Point> selectedPoints = FindPoints(points);

        // Create a copy of the start dna element
        NucleotidePair dummyElement = *fpStartBasicElement;

        // Loop on all the dna elements of the linker
        for(int p=0, pEnd=selectedPoints.size(); p<pEnd; ++p)
        {
            // Get the current point
            Point currentP = selectedPoints[p];
            QVector3D currentPV(currentP.x, currentP.y, currentP.z);

            // Translate the dummy to the current point (voxel frame)
            QVector3D translationVector ;

            if(p == 0)
            {
                translationVector = currentPV - dummyElement.GetPosVectorInDirectParentFrame(GetModelMat() );
            }
            else
            {
                translationVector = currentPV -  GetLastAddedChild()->GetPosVectorInParentFrame(this);
            }

            dummyElement.TranslateInFrame(translationVector, GetModelMat() );

            // Determine the angle between the current z axis and the tan vector of the next point
            // to orientate the dummy.

            // First get the current dna element z axis in the frame of the voxel
            QVector3D zCurrentAxis = ConvertToLocalFromFrame(zAxis, dummyElement.GetModelMat());

            // Get the rotation axis as well as the angle, still in the frame of the voxel
            QVector3D rotationAxis = QVector3D::crossProduct(zCurrentAxis, currentP.fTan);
            float dot = QVector3D::dotProduct(zCurrentAxis, currentP.fTan);
            float angle = std::atan2(rotationAxis.length(), dot); // rad
            angle = angle/M_PI*180; // deg

            // Orientate
            dummyElement.RotateInFrame(angle, rotationAxis, GetModelMat() );
            //dummyElement.RotateInLocalFrame(angle, rotationAxis);

            // Rotate along the dummy z axis to build the double helix
            dummyElement.RotateInLocalFrame(float(dummyElement.GetPivotAngle()), QVector3D(0,0,1) );

            dummyElement.SetCopyNumber(fFirstElemCopyNum+p);

            // Add the dummy element to the container
            AddDNAElement(dummyElement);
        }

        fSizeIndicator = ( (endPos-startPos).length()/5 );

        // Set the generation flag to true
        fIsGenerated = true;
    }
}

void Linker::ApplyPivotOffset(float angle)
{
    // Loop on all the DNA elements
    for(unsigned int element=0, elementEnd=fDNAElementIndex.size(); element<elementEnd; ++element)
    {
        GetDNAElement(element).RotateInLocalFrame(angle, QVector3D(0,0,1) );
    }
}

NucleotidePair &Linker::GetDNAElement(int index)
{
    if(!fIsGenerated)
    {
        SV::Utils::Exception(SV::Utils::ExceptionType::Fatal, "Linker::GetDNAElement", "Generate method was not called.");
    }

    return dynamic_cast<NucleotidePair&>(*GetChild(fDNAElementIndex[index]) );
}

void Linker::SetFirstElementCopyNumber(int copyNumber)
{
    fFirstElemCopyNum = copyNumber;

    // Loop on all the DNA elements of the linker and assign them their copy number
    for(unsigned int i=0, iEnd=fDNAElementIndex.size(); i<iEnd; ++i)
    {
        GetDNAElement(i).SetId(fFirstElemCopyNum+i);
    }
}

void Linker::Clear()
{
    ClearDNAElements();

    if(fpStartBasicElement != 0)
    {
        fpStartBasicElement = 0;
    }

    if(fpEndBasicElement != 0)
    {
        fpEndBasicElement = 0;
    }
}

void Linker::AddDNAElement(NucleotidePair &element)
{
    // Create a new object and a pointer to it
    NucleotidePair* el = new NucleotidePair(element);

    // Add the pointer to the childs.
    // It will be deleted when the geometrical object is destroyed.
    AddChild(el);

    fDNAElementIndex.push_back(GetNumOfChildren()-1);
}

std::vector<Point> Linker::BezierCurve(Point p0, Point p1, Point p2, Point p3, int pNumber)
{
    // This a method to calculate and save points coming from a cubic bezier curve.

    // Container to be returned with all the points
    std::vector<Point> output;

    output.resize(pNumber);

    // t value starts at 0
    float t(0.);

    // Value to be added to t
    float it(1./pNumber);

    // Loop on all the point to be generated on the bezier curve
    for(int i=0; i<pNumber; ++i)
    {
        // Cubic bezier equation
        Point p = p0*std::pow(1-t, 3)  + p1*3*t*std::pow(1-t,2) + p2*3*std::pow(t,2)*(1-t) + p3*std::pow(t,3);

        // Tan (first derivative)
        Point tanP = (p1-p0)*3*std::pow(1-t,2) + (p2-p1)*6*(1-t)*t + (p3-p2)*3*t*t ;
        p.fTan.setX(tanP.x);
        p.fTan.setY(tanP.y);
        p.fTan.setZ(tanP.z);
        p.fTan.normalize();p.fTan.normalize();

        // Add the generated point to the ouptut vector
        output[i] = p;

        // Define the next t value.
        // It can go from 0 to 1.
        t += it;
    }

    // Return
    return output;
}

float Linker::DistanceBtwPoints(const Point& p1, const Point& p2)
{
    Point p = p2-p1;

    float dx = p.x;
    float dy = p.y;
    float dz = p.z;

    float distance = std::sqrt( std::pow(dx,2) + std::pow(dy,2) + std::pow(dz,2) );

    return distance;
}

std::vector<Point> Linker::FindPoints(std::vector<Point> &points)
{
    std::vector<Point> selectedPoints;

    NucleotidePair dummy;
    float limit = dummy.GetDistance();

    std::vector<Point>::const_iterator currentP = points.begin();
    std::vector<Point>::const_iterator lastP = points.end();
    std::vector<Point>::const_iterator testP = points.begin();

    float distance;

    // Loop on all the dna elements to be included within the linker
    while(testP != lastP)
    {
        distance = 0;

        while (distance < limit && testP != lastP)
        {
            ++testP;

            distance = DistanceBtwPoints(*currentP, *testP);
        }

        if(testP != lastP)
        {
            selectedPoints.push_back(*testP);

            currentP = testP;
        }

        // If a max element number has been set then we quit after having it.
        if(fMaxElemNumber>0 && static_cast<int>(selectedPoints.size() )>=fMaxElemNumber) break;
    }

    // Remove the last element because it is litteraly "on" the first element of the next nucleosome.
    // This was expected and the removal is the best answer I guess.
    //
    selectedPoints.pop_back();

    fElemNumber = selectedPoints.size();

    return selectedPoints;
}

void Linker::ClearDNAElements()
{
    RemoveAllChildren();

    fDNAElementIndex.clear();
}

}}}
