#include "../include/NucleotidePair.hh"

namespace Models { namespace DNA { namespace Molecular {

using namespace SV;

NucleotidePair::NucleotidePair() : SceneNodeCRTP(),
    fPhosphate1Index(-1),
    fDeoxyribose1Index(-1),
    fBase1Index(-1),
    fBase2Index(-1),
    fDeoxyribose2Index(-1),
    fPhosphate2Index(-1),
    fDistance(0.331412), // nm
    fPivotAngle(36), // deg
    fIsDnaMaterial(true)
{
    SetName("NucleotidePair");
    SetType("NucleotidePair");

    Molecule* phosphate1 = new Molecule("phosphate1");
    Molecule* deoxyribose1 = new Molecule("deoxyribose1");
    Molecule* base1 = new Molecule("base1");
    Molecule* base2 = new Molecule("base2");
    Molecule* deoxyribose2 = new Molecule("deoxyribose2");
    Molecule* phosphate2 = new Molecule("phosphate2");

    // 1. Radius of DNA molecules.
    // 2. Radius of hydration layers of the DNA molecules.
    // 2. DNA molecule positions in the basic element.

    // Scaling factor corresponding to nanometer
    fFactor = 1.;

    // DNA molecule radius
    //
    phosphate1->SetRadius(0.270*fFactor);
    deoxyribose1->SetRadius(0.290*fFactor);
    base1->SetRadius(0.3*fFactor);
    base2->SetRadius(0.3*fFactor);
    deoxyribose2->SetRadius(0.290*fFactor);
    phosphate2->SetRadius(0.270*fFactor);

    // DNA hydration layers
    //
    double ratio = 1.7;

    phosphate1->SetWaterRadius(0.270*fFactor*ratio);
    deoxyribose1->SetWaterRadius(0.290*fFactor*ratio);
    base1->SetWaterRadius(0.3*fFactor*ratio);
    base2->SetWaterRadius(0.3*fFactor*ratio);
    deoxyribose2->SetWaterRadius(0.290*fFactor*ratio);
    phosphate2->SetWaterRadius(0.270*fFactor*ratio);

    // Strand

    phosphate1->SetStrand(1);
    deoxyribose1->SetStrand(1);
    base1->SetStrand(1);
    base2->SetStrand(2);
    deoxyribose2->SetStrand(2);
    phosphate2->SetStrand(2);

    // DNA molecule positions for a basic element
    //
    QVector3D pos;

    pos.setX(0.863185*fFactor);
    pos.setY(-0.209463*fFactor);
    pos.setZ(-0.211519*fFactor);
    phosphate1->SetPosition(pos);

    pos.setX(0.687466*fFactor);
    pos.setY(0.136434*fFactor);
    pos.setZ(-0.103885*fFactor);
    deoxyribose1->SetPosition(pos);

    pos.setX(0.334746*fFactor);
    pos.setY(-0.159769*fFactor);
    pos.setZ(-0.0371322*fFactor);
    base1->SetPosition(pos);

    pos.setX(-0.270308*fFactor);
    pos.setY(-0.0308147*fFactor);
    pos.setZ(0.0272545*fFactor);
    base2->SetPosition(pos);

    pos.setX(-0.712485*fFactor);
    pos.setY(0.235113*fFactor);
    pos.setZ(0.114808*fFactor);
    deoxyribose2->SetPosition(pos);

    pos.setX(-0.944741*fFactor);
    pos.setY(-0.0830581*fFactor);
    pos.setZ(0.218929*fFactor);
    phosphate2->SetPosition(pos);

    AddChild(phosphate1);
    fPhosphate1Index = GetNumOfChildren()-1;
    AddChild(deoxyribose1);
    fDeoxyribose1Index = GetNumOfChildren()-1;
    AddChild(base1);
    fBase1Index = GetNumOfChildren()-1;
    AddChild(base2);
    fBase2Index = GetNumOfChildren()-1;
    AddChild(deoxyribose2);
    fDeoxyribose2Index = GetNumOfChildren()-1;
    AddChild(phosphate2);
    fPhosphate2Index = GetNumOfChildren()-1;

    // *************************************************
    // Hydration layer
    // *************************************************

//    SV::Run::SceneNode* pW1 = new SV::Run::SceneNode(phosphate1->GetName()+"_hydrationLayer");
//    SV::Run::SceneNode* dW1 = new SV::Run::SceneNode(deoxyribose1->GetName()+"_hydrationLayer");
//    SV::Run::SceneNode* bW1 = new SV::Run::SceneNode(base1->GetName()+"_hydrationLayer");
//    SV::Run::SceneNode* bW2 = new SV::Run::SceneNode(base1->GetName()+"_hydrationLayer");
//    SV::Run::SceneNode* dW2 = new SV::Run::SceneNode(deoxyribose2->GetName()+"_hydrationLayer");
//    SV::Run::SceneNode* pW2 = new SV::Run::SceneNode(phosphate2->GetName()+"_hydrationLayer");

//    phosphate1->AddChild(pW1);
//    deoxyribose1->AddChild(dW1);
//    base1->AddChild(bW1);
//    base2->AddChild(bW2);
//    deoxyribose2->AddChild(dW2);
//    phosphate2->AddChild(pW2);

    // *************************************************

    // Use to set the 3dType of the volumes
    AssignDnaMaterial();

    DefineLods();
}

NucleotidePair::NucleotidePair(const NucleotidePair &toBeCopied) : SceneNodeCRTP(toBeCopied)
{
    fPhosphate1Index = toBeCopied.fPhosphate1Index;
    fDeoxyribose1Index = toBeCopied.fDeoxyribose1Index;
    fBase1Index = toBeCopied.fBase1Index;
    fBase2Index = toBeCopied.fBase2Index;
    fDeoxyribose2Index = toBeCopied.fDeoxyribose2Index;
    fPhosphate2Index = toBeCopied.fPhosphate2Index;
    fDistance = toBeCopied.fDistance;
    fPivotAngle = toBeCopied.fPivotAngle;
    fFactor = toBeCopied.fFactor;
    fIsDnaMaterial = toBeCopied.fIsDnaMaterial;

}

NucleotidePair &NucleotidePair::operator=(NucleotidePair rhs)
{
    SceneNodeCRTP::operator=(rhs);

    std::swap(fPhosphate1Index,rhs.fPhosphate1Index);
    std::swap(fDeoxyribose1Index,rhs.fDeoxyribose1Index);
    std::swap(fBase1Index,rhs.fBase1Index);
    std::swap(fBase2Index,rhs.fBase2Index);
    std::swap(fDeoxyribose2Index,rhs.fDeoxyribose2Index);
    std::swap(fPhosphate2Index,rhs.fPhosphate2Index);
    std::swap(fDistance,rhs.fDistance);
    std::swap(fPivotAngle,rhs.fPivotAngle);
    std::swap(fFactor,rhs.fFactor);
    std::swap(fIsDnaMaterial,rhs.fIsDnaMaterial);

    return *this;
}

NucleotidePair::~NucleotidePair()
{

}

void NucleotidePair::SetCopyNumber(int copyNumber)
{
    for(unsigned int i=0; i<GetNumOfChildren(); i++)
    {
        GetChild(i)->SetId(copyNumber);
    }

    fId=copyNumber;

    // If the dna material flag is set to true then we assign dna materials.
    // Base materials depend on the copy number (odd or even).
    if(fIsDnaMaterial)
    {
        AssignDnaMaterial();
        //SetName("t_"+fId);
        DefineLods();
    }
}

void NucleotidePair::SetModelMat(QMatrix4x4 mat)
{
    SV::Utils::Exception(SV::Utils::ExceptionType::Fatal,
                     "NucleotidePair::SetModelMat",
                     "Cannot be used here since the DNA element will not follow");
}

void NucleotidePair::AssignDnaMaterial()
{
    QColor green(0, 255, 0);
    //QColor blue(0, 0, 255);
    //QColor blueAlpha(0, 0, 255, 80);
    QColor yellow(255, 255, 0);
    QColor orange(255, 165, 0);
    //QColor pink(255, 0, 255);

    Molecule& p1 = dynamic_cast<Molecule&>(*GetChild(fPhosphate1Index) );
    p1.SetMaterial("homogeneous_dna");
    fMoleculeColor[&p1] = yellow;

    Molecule& d1 = dynamic_cast<Molecule&>(*GetChild(fDeoxyribose1Index) );
    d1.SetMaterial("homogeneous_dna");
    fMoleculeColor[&d1] = orange;


    if(fId%2==0)
    {
        Molecule& b1 = dynamic_cast<Molecule&>(*GetChild(fBase1Index) );
        b1.SetMaterial("homogeneous_dna");
        b1.SetName("base_adenine"); //b1.GetChild(0)->SetName("base_adenine_hydra");
        fMoleculeColor[&b1] = green;

        Molecule& b2 = dynamic_cast<Molecule&>(*GetChild(fBase2Index) );
        b2.SetMaterial("homogeneous_dna");
        b2.SetName("base_thymine"); //b2.GetChild(0)->SetName("base_thymine_hydra");
        fMoleculeColor[&b2] = green;
    }
    else
    {
        Molecule& b1 = dynamic_cast<Molecule&>(*GetChild(fBase1Index) );
        b1.SetMaterial("homogeneous_dna");
        b1.SetName("base_guanine"); //b1.GetChild(0)->SetName("base_guanine_hydra");
        fMoleculeColor[&b1] = green;

        Molecule& b2 = dynamic_cast<Molecule&>(*GetChild(fBase2Index) );
        b2.SetMaterial("homogeneous_dna");
        b2.SetName("base_cytosine"); //b2.GetChild(0)->SetName("base_cytosine_hydra");
        fMoleculeColor[&b2] = green;
    }

    Molecule& d2 = dynamic_cast<Molecule&>(*GetChild(fDeoxyribose2Index) );
    d2.SetMaterial("homogeneous_dna");
    fMoleculeColor[&d2] = orange;

    Molecule& p2 = dynamic_cast<Molecule&>(*GetChild(fPhosphate2Index) );
    p2.SetMaterial("homogeneous_dna");
    fMoleculeColor[&p2] = yellow;
}

void NucleotidePair::DefineLods()
{
    // Remove the nucleotide renderable component
    RemoveComponent(Logic::List::Renderable);

    // Remove the renderable component of each molecule object.
    // A renderable component contains all the information needed by the render system to display the object.
    for(unsigned int i=0; i<GetNumOfChildren(); i++)
    {
        GetChild(i)->RemoveComponent(Logic::List::Renderable);

        // For the hydration layer
        if(GetChild(i)->GetNumOfChildren()>0)
            GetChild(i)->GetChild(0)->RemoveComponent(Logic::List::Renderable);
    }

    // *************************************************************
    // DNA molecules LODs
    // *************************************************************

    // Get the molecules
    //
    Molecule& p1 = dynamic_cast<Molecule&>(*GetChild(fPhosphate1Index) );
    Molecule& d1 = dynamic_cast<Molecule&>(*GetChild(fDeoxyribose1Index) );
    Molecule& b1 = dynamic_cast<Molecule&>(*GetChild(fBase1Index) );
    Molecule& b2 = dynamic_cast<Molecule&>(*GetChild(fBase2Index) );
    Molecule& d2 = dynamic_cast<Molecule&>(*GetChild(fDeoxyribose2Index) );
    Molecule& p2 = dynamic_cast<Molecule&>(*GetChild(fPhosphate2Index) );

    // Add a renderable component to each molecule
    SV::Logic::Renderable* p1Renderable = new SV::Logic::Renderable();
    p1.AddComponent(p1Renderable, SV::Logic::List::Renderable);
    SV::Logic::Renderable* d1Renderable = new SV::Logic::Renderable();
    d1.AddComponent(d1Renderable, SV::Logic::List::Renderable);
    SV::Logic::Renderable* b1Renderable = new SV::Logic::Renderable();
    b1.AddComponent(b1Renderable, SV::Logic::List::Renderable);
    SV::Logic::Renderable* b2Renderable = new SV::Logic::Renderable();
    b2.AddComponent(b2Renderable, SV::Logic::List::Renderable);
    SV::Logic::Renderable* d2Renderable = new SV::Logic::Renderable();
    d2.AddComponent(d2Renderable, SV::Logic::List::Renderable);
    SV::Logic::Renderable* p2Renderable = new SV::Logic::Renderable();
    p2.AddComponent(p2Renderable, SV::Logic::List::Renderable);

    // Phosphate 1
    AutoSetLodForMol(p1, 8, 8, 100, p1Renderable); // 14 7
    AutoSetLodForMol(p1, 4, 4, 800, p1Renderable); // 6 3
    AutoSetLodForMol(p1, 4, 4, 2000, p1Renderable, true); // 6 3

    // Phosphate 2
    AutoSetLodForMol(p2, 8, 8, 100, p2Renderable); // 14 7
    AutoSetLodForMol(p2, 4, 4, 800, p2Renderable); // 6 3
    AutoSetLodForMol(p2, 4, 4, 2000, p2Renderable, true); // 6 3

    // Deoxyribose 1
    AutoSetLodForMol(d1, 8, 8, 100, d1Renderable);
    AutoSetLodForMol(d1, 4, 4, 800, d1Renderable);
    AutoSetLodForMol(d1, 4, 4, 2000, d1Renderable, true);

    // Deoxyribose 2
    AutoSetLodForMol(d2, 8, 8, 100, d2Renderable);
    AutoSetLodForMol(d2, 4, 4, 800, d2Renderable);
    AutoSetLodForMol(d2, 4, 4, 2000, d2Renderable, true);

    // Base 1
    AutoSetLodForMol(b1, 8, 8, 100, b1Renderable);
    AutoSetLodForMol(b1, 4, 4, 800, b1Renderable);
    AutoSetLodForMol(b1, 4, 4, 2000, b1Renderable, true);

    // Base 2
    AutoSetLodForMol(b2, 8, 8, 100, b2Renderable);
    AutoSetLodForMol(b2, 4, 4, 800, b2Renderable);
    AutoSetLodForMol(b2, 4, 4, 2000, b2Renderable, true);

    // *************************************************************
    // Nucleotide LODs
    // *************************************************************

    float sizeIndicator = 0.3*6*2 *fFactor;

    // Composite 0

    SV::Logic::Renderable* nucleotidePairRenderable = new SV::Logic::Renderable();
    AddComponent(nucleotidePairRenderable, SV::Logic::List::Renderable);

    SV::Logic::LodModel nucleotLod_0;
    nucleotLod_0.SetPrimitive(new SV::TriD::CompoObjPrimitive() );
    nucleotLod_0.SetDistance(50);
    nucleotLod_0.Set3dType("NucleotidePair_0");
    nucleotLod_0.SetIsMultiMesh(false);//true);
    nucleotLod_0.SetColorInstancing(false);
    nucleotLod_0.SetColor(QColor(0,255,0) );
    nucleotLod_0.SetShowChildren(true);//false);
    nucleotLod_0.SetChildForcedIndex(0);
    nucleotLod_0.SetSizeIndicator(sizeIndicator);
    nucleotLod_0.SetShaders(":/SymVox/3D/Shaders/multi_lights_instancing.vert",
                            ":/SymVox/3D/Shaders/multi_lights_instancing.frag");
    nucleotidePairRenderable->AddLod(nucleotLod_0);

    // Composite 1

    SV::Logic::LodModel nucleotLod_1 = nucleotLod_0;
    nucleotLod_1.SetDistance(60);
    nucleotLod_1.Set3dType("NucleotidePair_1");
    nucleotLod_1.SetChildForcedIndex(1);
    nucleotidePairRenderable->AddLod(nucleotLod_1);

    // Cube (1 of 2)

    // The idea here is to have two kinds of cube and two nullptr to use only 1 nuceotides of 2.
    // - yellow cube
    // - green cube
    // - 2 two nullptr

    int copyNumber = this->GetId();

    // Composite model
    SV::Logic::LodModel nucleotLod_2 = nucleotLod_0;
    nucleotLod_2.SetDistance(70);
    nucleotLod_2.Set3dType("NucleotidePair_2"); // Override just after
    nucleotLod_2.SetPrimitive(nullptr);
    nucleotLod_2.SetChildForcedIndex(3);
    nucleotLod_2.SetShowChildren(false);
    nucleotLod_2.SetIsMultiMesh(false);

    // Compute a flag from the copy number
    int flag = copyNumber%4;

    if(flag == 1) // Green cube
    {
        float size = 3*b1.GetRadius();
        SV::TriD::VPrimitive* cube = new SV::TriD::Cube(size);

        QColor color(0, 255, 0, 255);

        nucleotLod_2.SetPrimitive(cube);
        nucleotLod_2.SetColor(color);
        nucleotLod_2.Set3dType("NucleotidePair_2_Green");
    }
    else if(flag == 0 || flag == 2 ) // Nothing
    {
        nucleotLod_2.Set3dType("NucleotidePair_2_Invisible");
    }
    else // flag == 3: yellow cube
    {
        float size = 3*b1.GetRadius();
        SV::TriD::VPrimitive* cube = new SV::TriD::Cube(size);

        QColor color(255, 200, 0, 255);

        nucleotLod_2.SetPrimitive(cube);
        nucleotLod_2.SetColor(color);
        nucleotLod_2.Set3dType("NucleotidePair_2_Yellow");
    }

    nucleotidePairRenderable->AddLod(nucleotLod_2);

    // Nothing

    SV::Logic::LodModel nucleotLod_3 = nucleotLod_0;
    nucleotLod_3.SetDistance(420);
    nucleotLod_3.Set3dType("NucleotidePair_3");
    nucleotLod_3.SetPrimitive(nullptr);
    nucleotLod_3.SetChildForcedIndex(4);
    nucleotLod_3.SetShowChildren(false);
    nucleotLod_3.SetIsMultiMesh(false);
    nucleotLod_3.SetColor(QColor(200, 200, 0));

    nucleotidePairRenderable->AddLod(nucleotLod_3);
}

void NucleotidePair::AutoSetLodForMol(Molecule &m, int u, int v, float distance,
                                      SV::Logic::Renderable* renderable, bool visible)
{
    SV::Logic::LodModel lod;

    SV::TriD::Sphere* sphere = nullptr;

    if(visible)
        sphere = new SV::TriD::Sphere(QVector3D(), m.GetRadius(), u, v);

    std::ostringstream oss;
    // Phosphate_0, Phosphate_1 etc.
    oss << m.GetName() << "_" << renderable->GetNumOfLods();
    std::string name = oss.str();

    lod.Set3dType(name);
    lod.SetPrimitive(sphere);
    lod.SetSizeIndicator(m.GetRadius()*2);
    lod.SetColor(fMoleculeColor[&m]);
    lod.SetColorInstancing(true);
    lod.SetDistance(distance);
    lod.SetShowChildren(true);
    lod.SetShaders(":/SymVox/3D/Shaders/multi_lights_instancing_color.vert",
                   ":/SymVox/3D/Shaders/multi_lights_instancing_color.frag");

    renderable->AddLod(lod);

    // ******************************
    // Hydration layer
    // ******************************

//    SV::TriD::Sphere* sphereWater = new SV::TriD::Sphere(QVector3D(), m.GetWaterRadius(), u, v);

//    oss.str(""); oss.clear();
//    oss << "zzzz_" << m.GetName() << "_" << renderable->GetNumOfLods();
//    name = oss.str();

//    SV::Logic::LodModel lodWater = lod;
//    lodWater.Set3dType(name+"_hydrationLayer");
//    lodWater.SetPrimitive(sphereWater);
//    lodWater.SetColorInstancing(true);
//    lodWater.SetColor(QColor(0, 0, 255, 80) );
//    lodWater.SetSizeIndicator(m.GetWaterRadius()*2);

//    SV::Run::VSceneNode* hydrationLayerNode = m.GetChild(0);

//    if(hydrationLayerNode->GetRenderable() == nullptr)
//            hydrationLayerNode->SetRenderable(new SV::Logic::Renderable );

//    hydrationLayerNode->GetRenderable()->AddLod(lodWater);
}

}}}
