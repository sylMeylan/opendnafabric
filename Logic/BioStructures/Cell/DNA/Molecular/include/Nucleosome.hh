#ifndef NUCLEOSOME_HH
#define NUCLEOSOME_HH

#include "Molecule.hh"
#include "NucleotidePair.hh"

namespace Models { namespace DNA { namespace Molecular {

/**
 * @brief The Nucleosome class is combination of DNA molecules and a histone protein.
 *
 * A nucleosome is basically a entity containing 4 histone proteins in its center. These are attached together
 * and provide anchors points for a DNA fiber. This DNA fiber is helicoidally wrapped aroung the histone protein complex.
 * In the end, the addition of the histone proteines and the DNA fiber is commonly called a nucleosome.
 * In DnaFabric, there is also an Models::DNA::Molecular::Linker that can be considered as a part of the nucleosome.
 * It is a part of the DNA fiber used to provide a link between to consecutive nucleosomes. Thus, a continuous chain of DNA is
 * formed with the placement of several nucleosomes.
 *
 * Research suggests that nucleosome are organised in continous cylindrical fibers. On fiber, from its start to its end, being the DNA content of a whole
 * chromosome.
 */
class Nucleosome : public SV::Run::SceneNodeCRTP<SV::Run::VSceneNode, Nucleosome>
{
public:
    Nucleosome(int nucleosomeCopyNumber=0, int firstElementCopyNumber=0);
    ~Nucleosome();

    Nucleosome(const Nucleosome& toBeCopied);

    NucleotidePair& GetFirstDNAElement();
    NucleotidePair& GetLastDNAElement();
    int GetInitialElementNumber() const {return fInitialElementNumber;}
    int GetElementNumber() const {return fDNAElementIndex.size();}
    bool GetIsGenerated() const {return fIsGenerated;}
    bool GetIsNucleoAdjustable() const {return fIsNuclAdjustable;}
    NucleotidePair& GetDNAElement(int index);
    const NucleotidePair& GetDNAElement(int index) const;
    Molecule& GetHistone();
    const Molecule& GetHistone() const;
    void SetFirstElementCopyNum(int firstElementCopyNum);
    void SetNumberOfDNAElements(int num) {fInitialElementNumber=num;}
    void SetCopyNumber(int copyNum) {fId=copyNum; GetHistone().SetId(copyNum);}
    void SetIsNucleoAdjustable(bool b) {fIsNuclAdjustable=b;}
    void AddDNAElementFromLast(int num);
    void RemoveDNAElementFromLast(int num);
    void ApplyPivotOffset(float angle);
    void GenerateNucleosome();

protected:
    int fInitialElementNumber;
    int fFirstElementCopyNum;
    bool fIsGenerated;
    bool fIsNuclAdjustable;
    int fHistoneIndex;
    float fSizeIndiator;
    std::vector<int> fDNAElementIndex;

    void AddDNAElement(NucleotidePair& element);
    void AddHistone(Molecule& histone);
    void RemoveDNAElement(int index);
    void RemoveHistone();

    void DefineLods();
    void AutoSetLodForMol(Molecule& m, int u, int v, float distance,
                          SV::Logic::LodModel& lod, bool visible=true);
};

}}}

#endif // NUCLEOSOME_HH
