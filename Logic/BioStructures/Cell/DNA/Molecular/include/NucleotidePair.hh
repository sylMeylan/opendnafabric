#ifndef NUCLEOTIDEPAIR_HH
#define NUCLEOTIDEPAIR_HH

#include "SymVox/Run/Scene/Graph/include/SceneNodeCRTP.hh"
#include "SymVox/Run/Scene/Graph/include/SceneNode.hh"
#include "Molecule.hh"
#include "SymVox/3D/Geometry/include/Cube.hh"
#include "SymVox/3D/Geometry/include/Sphere.hh"
#include "SymVox/3D/Geometry/include/CompoObjPrimitive.hh"
#include "SymVox/Logic/Component/Renderable/include/LodModel.hh"
#include "SymVox/Logic/Component/Renderable/include/Renderable.hh"

namespace Models { namespace DNA { namespace Molecular {

/**
 * @brief The NucleotidePair class define a DNA element in DnaFabric.
 *
 * Molecules are the base unit of DnaFabric geometries and NucleotidePair are what comes just after.
 *
 * A nucleotide is made of 6 "DNA molecules" (Models::DNA::Molecular::Molecule):
 *  -# Phosphate 1
 *  -# Deoxyribose 1
 *  -# Base 1 (A, T, G, C...)
 *  -# Base 2 (A, T, G, C...)
 *  -# Deoxyribose 2
 *  -# Phosphate 2
 *  .
 * The deoxyribose and phosphate couples form the so-called "backbone" of the DNA double helix.
 */
class NucleotidePair : public SV::Run::SceneNodeCRTP<SV::Run::VSceneNode, NucleotidePair>
{
public:
    NucleotidePair();
    NucleotidePair(const NucleotidePair& toBeCopied);
    NucleotidePair& operator=(NucleotidePair rhs);
    ~NucleotidePair();

    // Getters

    Molecule& GetPhosphate1() {return dynamic_cast<Molecule&>(*GetChild(fPhosphate1Index) );}
    Molecule& GetDeoxyribose1() {return dynamic_cast<Molecule&>(*GetChild(fDeoxyribose1Index) );}
    Molecule& GetBase1() {return dynamic_cast<Molecule&>(*GetChild(fBase1Index) );}
    Molecule& GetBase2() {return dynamic_cast<Molecule&>(*GetChild(fBase2Index) );}
    Molecule& GetDeoxyribose2() {return dynamic_cast<Molecule&>(*GetChild(fDeoxyribose2Index) );}
    Molecule& GetPhosphate2() {return dynamic_cast<Molecule&>(*GetChild(fPhosphate2Index) );}

    const Molecule& GetPhosphate1() const {return dynamic_cast<const Molecule&>(*GetChild(fPhosphate1Index) );}
    const Molecule& GetDeoxyribose1() const {return dynamic_cast<const Molecule&>(*GetChild(fDeoxyribose1Index) );}
    const Molecule& GetBase1() const {return dynamic_cast<const Molecule&>(*GetChild(fBase1Index) );}
    const Molecule& GetBase2() const {return dynamic_cast<const Molecule&>(*GetChild(fBase2Index) );}
    const Molecule& GetDeoxyribose2() const {return dynamic_cast<const Molecule&>(*GetChild(fDeoxyribose2Index) );}
    const Molecule& GetPhosphate2() const {return dynamic_cast<const Molecule&>(*GetChild(fPhosphate2Index) );}

    double GetScaleFactor() const {return fFactor;}
    float GetDistance() const {return fDistance;}
    float GetPivotAngle() const {return fPivotAngle;}
    bool GetIsDnaMaterial() const {return fIsDnaMaterial;}

    // Setters

    void SetScaleFactor(double factor) {fFactor = factor;}
    void SetCopyNumber(int copyNumber);
    void SetModelMat(QMatrix4x4 mat);
    void SetIsDnaMaterial(bool b) {fIsDnaMaterial=b;}

    void AssignDnaMaterial();

private:
    // DNA molecules in one basic "DNA element"
    //
    int fPhosphate1Index;
    int fDeoxyribose1Index;
    int fBase1Index;
    int fBase2Index;
    int fDeoxyribose2Index;
    int fPhosphate2Index;

    float fDistance;
    float fPivotAngle;

    double fFactor;

    bool fIsDnaMaterial;

    std::map<Molecule*, QColor> fMoleculeColor;

    void DefineLods();
    void AutoSetLodForMol(Molecule& m, int u, int v, float distance,
                          SV::Logic::Renderable *renderable, bool visible=true);
};

}}}

#endif // NUCLEOTIDEPAIR_HH
