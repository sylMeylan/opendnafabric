#ifndef LINKER_HH
#define LINKER_HH

#include "NucleotidePair.hh"
#include "SymVox/Run/Scene/Graph/include/SceneNodeCRTP.hh"

namespace Models { namespace DNA { namespace Molecular {

class Point;

/**
 * @brief The Linker class
 *
 * A linker a set of DNA elements that link together two nucleosomes.
 */
class Linker : public SV::Run::SceneNodeCRTP<SV::Run::VSceneNode, Linker>
{
public:
    Linker();
    Linker(NucleotidePair& startElement, NucleotidePair& endElement, VSceneNode* parentNode);
    ~Linker();
    Linker(const Linker& toBeCopied);
    Linker& operator=(Linker toBeCopied);

    void Generate();

    void ApplyPivotOffset(float angle);

    bool GetIsGenerated() const {return fIsGenerated;}
    NucleotidePair& GetStartElement() const {return *fpStartBasicElement;}
    NucleotidePair& GetEndElement() const {return *fpEndBasicElement;}
    int GetElementNumber() const {return fElemNumber;}
    int GetFirstElementCopyNumber() const {return fFirstElemCopyNum;}
    NucleotidePair& GetDNAElement(int index);
    int GetBezierCurveNbP() {return fBezierCurveNbP;}
    int GetMaxElemNum() {return fMaxElemNumber;}
    float GetControlPointDistance1() {return fControlPointDistance1;}
    float GetControlPointDistance2() {return fControlPointDistance2;}

    void SetStartElement(NucleotidePair& element){fpStartBasicElement=&element;}
    void SetEndElement(NucleotidePair& element){fpEndBasicElement=&element;}
    void SetFirstElementCopyNumber(int copyNumber);
    void SetElementNumber(int num) {fElemNumber=num;}
    void SetIsGenerated(bool b) {fIsGenerated=b;}
    void SetBezierCurveNbP(int num) {fBezierCurveNbP=num;}
    void SetMaxElemNum(int num) {fMaxElemNumber=num;}
    void SetControlPointDistance1(float distance) {fControlPointDistance1=distance;}
    void SetControlPointDistance2(float distance) {fControlPointDistance2=distance;}
    void SetControlPointOptVec1(QVector3D vect) {fControlPointOptionalVect1=vect;}
    void SetControlPointOptVec2(QVector3D vect) {fControlPointOptionalVect2=vect;}

    void Clear();

private:
    bool fIsGenerated;
    int fFirstElemCopyNum;
    int fElemNumber;
    int fBezierCurveNbP;
    int fMaxElemNumber;
    float fControlPointDistance1;
    float fControlPointDistance2;
    float fSizeIndicator;
    QVector3D fControlPointOptionalVect1;
    QVector3D fControlPointOptionalVect2;
    NucleotidePair* fpStartBasicElement;
    NucleotidePair* fpEndBasicElement;
    VSceneNode* fpVoxel;
    std::vector<int> fDNAElementIndex;

    void AddDNAElement(NucleotidePair &element);
    std::vector<Point> BezierCurve(Point p0, Point p1, Point p2, Point p3, int pNumber);
    float DistanceBtwPoints(const Point &p1, const Point &p2);
    std::vector<Point> FindPoints(std::vector<Point> &points);
    void ClearDNAElements();
};

}}}

#endif // LINKER_HH

#ifndef DNAPOINT_HH
#define DNAPOINT_HH

namespace Models { namespace DNA { namespace Molecular {

class Point
{
public:
    Point(){x=0;y=0;z=0;}
    Point(float a, float b, float c){x=a; y=b; z=c;}
    ~Point(){;}

    float x;
    float y;
    float z;

    QVector3D fTan;

    Point& operator+=(const Point& p1)
    {
        x += p1.x;
        y += p1.y;
        z += p1.z;

        return *this;
    }

    Point& operator-=(const Point& p1)
    {
        x -= p1.x;
        y -= p1.y;
        z -= p1.z;

        return *this;
    }

    template<class T>
    Point& operator*=(const T& f)
    {
        x *= f;
        y *= f;
        z *= f;

        return *this;
    }

    template<class T>
    Point& operator/=(const T& f)
    {
        x /= f;
        y /= f;
        z /= f;

        return *this;
    }
};

inline Point operator+(Point p1, const Point& p2)
{
    p1 += p2;

    return p1;
}

inline Point operator-(Point p1, const Point& p2)
{
    p1 -= p2;

    return p1;
}

template<class T>
inline Point operator*(Point p1, const T& f2)
{
    p1 *= f2;

    return p1;
}

template<class T>
inline Point operator/(Point p1, const T& f2)
{
    p1 /= f2;

    return p1;
}

}}}

#endif // POINT_HH
