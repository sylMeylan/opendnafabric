#ifndef MOLECULE_HH
#define MOLECULE_HH

#include <string>
#include "SymVox/Run/Scene/Graph/include/SceneNodeCRTP.hh"

namespace Models { namespace DNA { namespace Molecular {

/**
 * @brief The Molecule class represents a DNAmolecule
 *
 * A molecule has a spherical shape, usually computed from the VanDerWaals radii of its atoms, in DnaFabric.
 * It also has and a set of variables to uniquely identify it:
 *  - Type
 *  - Name
 *  - Id or CopyNumber
 *
 * Please, note that the DnaFabric definition of a molecule is not always the same as the biological one. Indeed, in DnaFabric
 * a molecule should be seen a basic construction unit. For example, in biology, a DNA molecule is a whole chromosome whereas
 * it is a base or a deoxyribose or a phosphate group in DnaFabric.
 */
class Molecule : public SV::Run::SceneNodeCRTP<SV::Run::VSceneNode, Molecule>
{
public:
    Molecule();
    Molecule(const std::string& name);
    Molecule(const std::string& name, double radius);
    Molecule(const std::string& name, double radius, QMatrix4x4 &posAndRot);
    ~Molecule();

    Molecule(const Molecule& toBeCopied);
    Molecule& operator=(Molecule rhs);

    // Setters
    void SetRadius(double radius) {fRadius=radius;}
    void SetWaterRadius(double radius) {fWaterRadius=radius;}
    void SetMaterial(const std::string& mat) {fMaterial = mat;}
    void SetStrand(int strand) {fStrand = strand;}

    // Getters
    double GetRadius() const {return fRadius;}
    double GetWaterRadius() const {return fWaterRadius;}
    std::string GetMaterial() const {return fMaterial;}
    int GetStrand() const {return fStrand;}

protected:
    double fRadius; ///< VanDerWaals radius of the molecule
    double fWaterRadius; ///< Radius of a sphere including the molecule and the hydration layer
    int fStrand; ///< DNA strand
    std::string fMaterial; ///< DNA Material
};

}}}

#endif // MOLECULE_HH
