#ifndef VVOXEL_HH
#define VVOXEL_HH

#include "Logic/BioStructures/Cell/DNA/Molecular/include/Nucleosome.hh"
#include "Logic/BioStructures/Cell/DNA/Molecular/include/Linker.hh"
#include "SymVox/Run/Scene/Graph/include/SceneNode.hh"

namespace Models { namespace DNA { namespace Molecular {

/**
 * @brief The VVoxel class provides a base class for every DNA voxel.
 *
 * A voxel is a cube containing an amount of DNA molecules.
 * Usually, these are organized in Nucleosome, NucleotidePair and Linker.
 *
 * @note A DNA element refers to NucleotidePair.
 *
 * @extends SV::Run::SceneNode
 */
class VVoxel :
        #ifndef DOXYGEN_SHOULD_SKIP_THIS
        public SV::Run::SceneNodeCRTP<SV::Run::SceneNode, VVoxel>
        #endif
{
public:
    VVoxel(const std::string& name); ///< Constructor
    virtual ~VVoxel(); ///< Destructor

    virtual void Generate() {;} ///< @brief Function with the code required to fill the voxel with DNA

    void UpdateStartLinker(); ///< @brief Function to adapt the start linker to the content of the first nucleosome
    void UpdateEndLinker(); ///< @brief Function to adapt the end linker to the content of the last nucleosome
    Linker& GetStartLinker(); ///< @brief Get the start linker
    Linker& GetEndLinker(); ///< @brief Get the end linker
    float GetLength(){return fVoxelLength;} ///< @brief Return The voxel length
    const NucleotidePair& GetStartDNAElement() const {return fStartDNAElement;} ///< @brief Get the Start DNA element
    const NucleotidePair& GetEndDNAElement() const {return fEndDNAElement;} ///< @brief Get the End DNA element
    void SetStartDNAElement(NucleotidePair& el) {fStartDNAElement=el;} ///< @brief Set the start DNA element
    void SetEndDNAElement(NucleotidePair& el) {fEndDNAElement=el;} ///< @brief Set the End DNA element
    void SetLength(float length) {fVoxelLength=length;} ///< @brief Set the length of the voxel
    void Reset(); ///< @brief Empty the voxel and reset its parameters (if any)
    void ResetSpecialDnaObject(); ///< @brief Special reset used to only reset the linker (start and stop)
    void SetDNAElemPerNucleAndLink(int num) {fDNAElementPerNucleoAndLink=num;} ///< @brief The number of DNA element per nucleosome and linker couple.
    int GetNucleosomeNumber() const {return fNucleosomeIndex.size();} ///< @brief Get the number of nucleosome
    int GetLinkerNumber() const {return fLinkerIndex.size();} ///< @brief Get the number of linker in the voxel
    int GetDNAElemPerNuclAndLink() const {return fDNAElementPerNucleoAndLink;} ///<@brief Get the number of DNA elements per nucleosome and linker couple.
    Nucleosome& GetNucleosome(int index); ///< @brief Get the nucleosome at @p index.
    Linker& GetLinker(int index); ///< @brief Get the linker at @p index
    void UpdateLinker(int linkerIndex, Nucleosome& nuclBefore, Nucleosome& nuclAfter); ///< @brief Update the linker with the index @p index that "links" the two given nucleosomes.
    void UpdateLinker(Linker& linker, NucleotidePair& startElem, NucleotidePair& endElem, ///< @brief Update the linker.
                             int updateIndex, Nucleosome* nuclAfter=0);
    virtual void AddCopyOfNucleosome(Nucleosome &nucleosome); ///< @brief Add a copy of @p nucleosome
    void SetLodColor(const QColor& color); ///< @brief Set the color of all the lod of the current object

    /*!
     * \brief
     * \param linker
     * \param nucleosome
     * Used to add or remove dna elements from one nucleosome in function of the size of its linker.
     * The more its linker is big, the more we remove dna element from the nucleosome.
     * Small linker means adding dna element.
     * The goal is to maintain a constant linker+nucleosome dna element number.
     */
    void AdjustElemNumber(Linker& linker, Nucleosome& nucleosome);

    int GetNumOfNucleotides(); ///< @brief Return the number of nucleotides inside the voxel
    int GetFirstDNAElemCopyNumber() {return fFirstDNAElementInNuclCopyNumber;} ///< @brief Return the copy-number (alias id) of the first DNA element

protected:
    float fVoxelLength; ///< @brief Length of the voxel (cubic)
    int fStartLinkerIndex; ///< @brief Index of the start linker
    int fEndLinkerIndex; ///< @brief Index of the end linker
    int fNucleosomeCopyNumber; ///< @brief Id of the first nucleosome
    int fFirstDNAElementInNuclCopyNumber; ///< @brief Id of the first DNA element of the voxel
    int fDNAElementPerNucleoAndLink; ///< @brief Number of DNA element per nucleosome and linker couple
    NucleotidePair fStartDNAElement; ///< @brief Start DNA element
    NucleotidePair fEndDNAElement; ///< @brief End DNA element
    std::vector<int> fNucleosomeIndex; ///< @brief Index of all the nucleosome of the voxel
    std::vector<int> fLinkerIndex; ///< @brief Index of all the linkers of the voxel

    void AddStartLinker(Linker& startLinker); ///< @brief Add a start linker
    void AddEndLinker(Linker& endLinker); ///< @brief Add and end linker
    void GenerateStartLinker(Nucleosome &startNucl); ///< @brief Fill the start linker with DNA elements
    void GenerateEndLinker(Nucleosome &endNucl); ///< @brief Fill the end linker with DNA elements
    bool IsInsideVoxel(Molecule &mol); ///< @brief Test is the given molecule (@p mol) is inside the voxel

    void AddNucleo(Nucleosome* nucleo); ///< @brief Add the given nucleosome
    void AddLinker(Linker* linker); ///< @brief Add the given linker
    void DefineLods(const QColor &color); ///< @brief Define the 3D representations of the voxel (LODs)
};

}}}

#endif // VVOXEL_HH
