#ifndef VOXELSTRAIGHT_HH
#define VOXELSTRAIGHT_HH

#include "VVoxel.hh"

namespace Models { namespace DNA { namespace Molecular {

/**
 * @brief The VoxelStraight class
 * @extends VVoxel
 */
class VoxelStraight :
        #ifndef DOXYGEN_SHOULD_SKIP_THIS
                public SV::Run::SceneNodeCRTP<VVoxel, VoxelStraight>
        #endif
{
public:
    VoxelStraight(const std::string& name);
    ~VoxelStraight();

    void SetIsGenerated(bool b) {fIsGenerated=b;}
    bool GetIsGenerated() const {return fIsGenerated;}
    void Generate() override final;
    void AnimateExtend(float factor);

private:
    bool fIsGenerated;
    float fFiberRadius;
    float fFiberDeltaAngle;
    float fFiberZShiftBtwNucl;
};

}}}

#endif // VOXELSTRAIGHT_HH
