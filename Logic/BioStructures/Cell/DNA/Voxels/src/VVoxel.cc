#include "../include/VVoxel.hh"

namespace Models { namespace DNA { namespace Molecular {

VVoxel::VVoxel(const std::string& name)
    : SV::Run::SceneNodeCRTP<SV::Run::SceneNode, VVoxel>(name)
{
    SetType("Voxel");

    fStartLinkerIndex = -1;
    fEndLinkerIndex = -1;

    SetLength(50.0f);//50.75; // 50

    // Nucleosome container part

    fNucleosomeCopyNumber = 0;
    fFirstDNAElementInNuclCopyNumber = 0;
    fDNAElementPerNucleoAndLink = 200;

    DefineLods(QColor(0,255,0));
}

VVoxel::~VVoxel()
{

}

void VVoxel::UpdateStartLinker()
{
    UpdateLinker(GetStartLinker(), fStartDNAElement, GetNucleosome(0).GetFirstDNAElement(), 0, &GetNucleosome(0));
}

void VVoxel::UpdateEndLinker()
{
    UpdateLinker(GetEndLinker(), GetNucleosome(fNucleosomeIndex.back() ).GetLastDNAElement(), fEndDNAElement, 0, 0);
}

Linker &VVoxel::GetStartLinker()
{
    VSceneNode& obj = *GetChild(fStartLinkerIndex);

    return dynamic_cast<Linker&>(obj);
}

Linker &VVoxel::GetEndLinker()
{
    VSceneNode& obj = *GetChild(fEndLinkerIndex);

    return dynamic_cast<Linker&>(obj);
}

void VVoxel::Reset()
{
    fNucleosomeIndex.clear();
    fLinkerIndex.clear();
    fFirstDNAElementInNuclCopyNumber=0;
    fNucleosomeCopyNumber=0;

    ResetSpecialDnaObject();

    RemoveAllChildren();
}

void VVoxel::ResetSpecialDnaObject()
{
    GetStartLinker().Clear();
    GetEndLinker().Clear();
}

Nucleosome &VVoxel::GetNucleosome(int index)
{
    // Nucleosome index leads to the corresponding child index
    VSceneNode& obj = *GetChild(fNucleosomeIndex[index]);

    return dynamic_cast<Nucleosome&>(obj);
}

Linker &VVoxel::GetLinker(int index)
{
    // Linker index leads to the corresponding child index
    VSceneNode& obj = *GetChild(fLinkerIndex[index]);

    return dynamic_cast<Linker&>(obj);
}

void VVoxel::UpdateLinker(int linkerIndex, Nucleosome &nuclBefore, Nucleosome &nuclAfter)
{
    Linker& linker = GetLinker(linkerIndex);

    UpdateLinker(linker, nuclBefore.GetLastDNAElement(), nuclAfter.GetFirstDNAElement(), linkerIndex+1, &nuclAfter);
}

void VVoxel::UpdateLinker(Linker &linker, NucleotidePair &startElem, NucleotidePair &endElem, int updateIndex, Nucleosome *nuclAfter)
{
    // Element in the old linker (before moving the nucleosome)
    int numOfElementInOldLinker = linker.GetElementNumber();

    // Set the linker start and end points
    linker.SetStartElement(startElem);
    linker.SetEndElement(endElem);

    // Get the distance between the end and start points
    QVector3D diffPos = startElem.GetPosVectorInParentFrame(this) // .GetPosVectorInFrame(linker.GetParent()->GetModelMat() )
            - endElem.GetPosVectorInParentFrame(this); // .GetPosVectorInFrame(linker.GetParent()->GetModelMat() );
    float dPos = diffPos.length();

    // 100 pts per distance unit.
    // This should ensure a good sampling on the bezier curve but only unti a
    // limit distance (to be determined... it should be far far away).
    // Indeed, the point distribution on a Bezier curve is not linear.
    linker.SetBezierCurveNbP(100*dPos);

    // Will auto clear the old dna basic elements and re-generate the linker
    linker.SetIsGenerated(false);
    linker.Generate();

    // Remove or add dna element to the nucleosome when the linker size changes
    if(nuclAfter) if(nuclAfter->GetIsNucleoAdjustable() ) AdjustElemNumber(linker, *nuclAfter);

    // Element in the newly computed linker
    int numOfElementInNewLinker = linker.GetElementNumber();

    // Compute the number of dna elements added or removed to perform the new link
    int diff = numOfElementInNewLinker - numOfElementInOldLinker;

    // If this number is not null then we need to readjust the copy number and the offset angle
    if(diff != 0 && nuclAfter != 0)
    {
        nuclAfter->SetFirstElementCopyNum(nuclAfter->GetFirstDNAElement().GetId()
                                         + diff);

        float angleOffset = std::fmod(diff*linker.GetDNAElement(0).GetPivotAngle(), 360);

        nuclAfter->ApplyPivotOffset(angleOffset);

        // Loop on all the following linkers and nucleosomes to adjust them
        for(int i=updateIndex, ie=fLinkerIndex.size(); i<ie; ++i)
        {
            Nucleosome& n1 = GetNucleosome(i);
            Nucleosome& n2 = GetNucleosome(i+1);

            Linker& link = GetLinker(i);

            link.SetFirstElementCopyNumber(n1.GetLastDNAElement().GetId()+1);
            link.ApplyPivotOffset(angleOffset);

            n2.SetFirstElementCopyNum(link.GetDNAElement(link.GetElementNumber()-1).GetId()+1);
            n2.ApplyPivotOffset(angleOffset);
        }
    }
}

void VVoxel::AddCopyOfNucleosome(Nucleosome &nucleosome)
{
    // Add a the nucleosome to the nucleosome container and auto set the
    // copy number parameters.

    Nucleosome* nucleo = new Nucleosome(nucleosome);

    // To add the nucleosome into the container
    AddNucleo(nucleo);

    // This will update the copy number of the nucleosome and of the histone within it.
    nucleo->SetCopyNumber(fNucleosomeCopyNumber);

    float angleOffset(0);

    // Look for a previously placed nucleosome
    if(fNucleosomeIndex.size() > 1)
    {
        // If there is one, then add a linker between the new and the former nucleosome

        Linker* linker = new Linker(GetNucleosome(GetNucleosomeNumber()-2).GetLastDNAElement(),
                                    GetNucleosome(GetNucleosomeNumber()-1).GetFirstDNAElement(),
                                    this);

        linker->Generate();

        if(nucleo->GetIsNucleoAdjustable() ) AdjustElemNumber(*linker, *nucleo);

        linker->SetId(fNucleosomeCopyNumber);

        linker->SetFirstElementCopyNumber(fFirstDNAElementInNuclCopyNumber);
        fFirstDNAElementInNuclCopyNumber += linker->GetElementNumber();

        AddLinker(linker);

        angleOffset = std::fmod(fFirstDNAElementInNuclCopyNumber*linker->GetDNAElement(0).GetPivotAngle(), 360);

        nucleo->ApplyPivotOffset(angleOffset);
    }

    ++fNucleosomeCopyNumber;

    // This will update the copy number of all the DNA elements contain within the nucleosome
    // and accordingly with the first element copy number.
    nucleo->SetFirstElementCopyNum(fFirstDNAElementInNuclCopyNumber);
    fFirstDNAElementInNuclCopyNumber += nucleo->GetElementNumber();
}

void VVoxel::SetLodColor(const QColor &color)
{
    SV::Logic::Renderable* renderable =
            static_cast<SV::Logic::Renderable*>(GetComponent(SV::Logic::List::Renderable));

    renderable->SetLodColorForAll(color);
}

void VVoxel::AdjustElemNumber(Linker &linker, Nucleosome &nucleosome)
{
    // This method enforces the constraint on the DNA element contained in the nucleosome AND the linker.

    int totElemNum = linker.GetElementNumber()+nucleosome.GetElementNumber();

    // Check if the tot number is different from what is expected

    int diff = totElemNum - fDNAElementPerNucleoAndLink;

    // Check if we have too many elements
    if(diff>0)
    {
        // We need to remove some elements

        // Check that we still have at least one dna element within the nucleosome before the removal

        if(nucleosome.GetElementNumber()-diff > 1)  nucleosome.RemoveDNAElementFromLast(diff);
        else
        {
            std::ostringstream oss;
            oss << "Nucleosome cannot be adjusted anymore because it already has minimal dna element number.";
            oss << "Linker elements: "<<linker.GetElementNumber()<<"; nucleosome element(s): "<<nucleosome.GetElementNumber();
            SV::Utils::Exception(SV::Utils::ExceptionType::Warning, "VVoxel::AdjustElemNumber", oss.str() );
        }
    }
    else if(diff<0)
    {
        // We need to add some elements
        nucleosome.AddDNAElementFromLast(-diff);
    }
}

int VVoxel::GetNumOfNucleotides()
{
    int num (0);

    num += GetStartLinker().GetElementNumber();
    num += GetEndLinker().GetElementNumber();

    num += GetNucleosomeNumber() * GetDNAElemPerNuclAndLink();

    return num;
}

void VVoxel::AddStartLinker(Linker &startLinker)
{
    Linker* linker = new Linker(startLinker);

    AddChild(linker);

    fStartLinkerIndex = GetNumOfChildren()-1;
}

void VVoxel::AddEndLinker(Linker &endLinker)
{
    Linker* linker = new Linker(endLinker);

    AddChild(linker);

    fEndLinkerIndex = GetNumOfChildren()-1;
}

void VVoxel::GenerateStartLinker(Nucleosome &startNucl)
{
    AddChild(&fStartDNAElement);

    int startIndex = GetNumOfChildren()-1;

    Linker linker(fStartDNAElement, startNucl.GetFirstDNAElement(), this );

    // Start linker

    linker.Generate();

    linker.SetId(startNucl.GetId());

    fFirstDNAElementInNuclCopyNumber = 0;
    linker.SetFirstElementCopyNumber(fFirstDNAElementInNuclCopyNumber);
    fFirstDNAElementInNuclCopyNumber += linker.GetElementNumber();

    fChildren.erase(fChildren.begin()+startIndex);

    AddStartLinker(linker);

    GetStartLinker().SetName("StartLinker");

    float angleOffset = std::fmod(fFirstDNAElementInNuclCopyNumber*linker.GetDNAElement(0).GetPivotAngle(), 360);

    startNucl.ApplyPivotOffset(angleOffset);
    startNucl.SetFirstElementCopyNum(fFirstDNAElementInNuclCopyNumber);
}

void VVoxel::GenerateEndLinker(Nucleosome &endNucl)
{
    AddChild(&fEndDNAElement);

    int startIndex = GetNumOfChildren()-1;

    // End linker

    fEndDNAElement.TranslateInLocalFrame(QVector3D(0,0,2*fEndDNAElement.GetDistance() ) );

    Linker linker(endNucl.GetLastDNAElement(), fEndDNAElement, this );

    fEndDNAElement.TranslateInLocalFrame(QVector3D(0,0,-fEndDNAElement.GetDistance() ) );

    linker.Generate();

    linker.SetId(endNucl.GetId()+1);

    linker.SetFirstElementCopyNumber(fFirstDNAElementInNuclCopyNumber);
    fFirstDNAElementInNuclCopyNumber += linker.GetElementNumber();

    fChildren.erase(fChildren.begin()+startIndex);

    AddEndLinker(linker);

    GetEndLinker().SetName("EndLinker");
}

bool VVoxel::IsInsideVoxel(Molecule &mol)
{
    bool output(false);

    // Get the molecule position in the voxel frame
    QVector3D pos = mol.GetPosVectorInParentFrame(this);// GetPosVectorInFrame(GetModelMat() );

    // Define the radius variable and test if a water radius is defined.
    // If yes, then use the water radius.
    float radius;
    if(mol.GetWaterRadius()==0) radius=mol.GetRadius();
    else radius = mol.GetWaterRadius();

    // Check if the molecule is inside or outside the voxel
    if( std::abs(pos.x() ) - radius < fVoxelLength
            && std::abs(pos.y() ) - radius < fVoxelLength
            && std::abs(pos.z() ) - radius < fVoxelLength ) output=true;

    return output;
}

void VVoxel::AddNucleo(Nucleosome *nucleo)
{
    // Add the nucleosome in the child container
    AddChild(nucleo);

    // Add the nucleosome child index within the nucleosome index container
    fNucleosomeIndex.push_back(GetNumOfChildren()-1);
}

void VVoxel::AddLinker(Linker *linker)
{
    // Add the linker in the child container
    AddChild(linker);

    // Add the linker child index within the linker index container
    fLinkerIndex.push_back(GetNumOfChildren()-1);
}

void VVoxel::DefineLods(const QColor& color)
{
    SV::Logic::Renderable* renderable = new SV::Logic::Renderable;
    AddComponent(renderable, SV::Logic::List::Renderable);

    SV::Logic::LodModel lod;
    lod.SetPrimitive(new SV::TriD::CompoObjPrimitive() );
    lod.SetIsMultiMesh(true);
    lod.SetColorInstancing(false);
    lod.SetShowChildren(false);
    lod.SetSizeIndicator(fVoxelLength);
    lod.SetShaders(":/SymVox/3D/Shaders/multi_lights_instancing.vert",
                        ":/SymVox/3D/Shaders/multi_lights_instancing.frag");

    // 1 Full details
    lod.SetDistance(50);
    lod.Set3dType(GetName()+"_0");
    lod.SetChildForcedIndex(0);
    renderable->AddLod(lod);

    // 2 Approx spheres
    lod.SetDistance(130);
    lod.Set3dType(GetName()+"_1");
    lod.SetChildForcedIndex(1);
    renderable->AddLod(lod);

    // 3 Cubes instead of nucleotides
    lod.SetDistance(800);
    lod.Set3dType(GetName()+"_2");
    lod.SetChildForcedIndex(2);
    renderable->AddLod(lod);

    // 4 Only big cubes for nucleosomes
    lod.SetDistance(1500);
    lod.Set3dType(GetName()+"_3");
    lod.SetChildForcedIndex(3);
    renderable->AddLod(lod);

    // 5 Voxel cube only
    lod.SetPrimitive( nullptr );//new SV::TriD::Cube(fVoxelLength) );
    lod.SetIsMultiMesh(false);
    lod.SetColorInstancing(true);
    lod.SetShowChildren(false);
    lod.SetColor(QColor(255, 0, 0, 80)); // red alpha
    lod.SetDistance(10000);
    lod.Set3dType("z_VoxelCube");
    lod.SetShaders(":/SymVox/3D/Shaders/multi_lights_instancing_color.vert",
                        ":/SymVox/3D/Shaders/multi_lights_instancing_color.frag");
    renderable->AddLod(lod);
}

}}}
