#include "../include/VoxelStraight.hh"

namespace Models { namespace DNA { namespace Molecular {

VoxelStraight::VoxelStraight(const std::string &name)
    : SV::Run::SceneNodeCRTP<VVoxel, VoxelStraight>(name),
      fIsGenerated(false)
{
    // 6 nucleosomes per turn
    // Pitch of 10 nm
    //
    fFiberRadius = 10.460;
    fFiberDeltaAngle = 360./6;
    //fFiberZShiftBtwNucl = 10./6;
    fFiberZShiftBtwNucl = 10.5/6;
    //fVoxelLength = fFiberZShiftBtwNucl*29; // 50.75 // nm

    Generate();
}

VoxelStraight::~VoxelStraight()
{

}

void VoxelStraight::Generate()
{
    if(fIsGenerated==false)
    {
        // Create a dummy nucleosome to save copy of it within the class object container

        // Create the nucleosome object
        Nucleosome dummyNucleosome(0, 0);

        // Do the maths to get all the nucleosome DNA molecules generated and placed within it
        dummyNucleosome.GenerateNucleosome();

        // Translate and rotate the dummy to place it at the right positions before to copy it

        // Go backward to the "start point" in the voxel
        QVector3D startPointTranslation(0, 0, -fVoxelLength/2 + 5 );
        dummyNucleosome.TranslateInFrame(startPointTranslation, GetModelMat() );

        // Go to the start point (left)
        QVector3D xTranslation(fFiberRadius,0,0);
        dummyNucleosome.TranslateInLocalFrame(xTranslation);

        // Set Start element
        fStartDNAElement.TranslateInFrame(QVector3D(fFiberRadius+2, -6, -fVoxelLength/2), GetModelMat() );
        // Set End element
        fEndDNAElement = fStartDNAElement;
        fEndDNAElement.TranslateInFrame(QVector3D(0,0,fVoxelLength), GetModelMat() );

        //GenerateStartLinker(dummyNucleosome);

        // Add the first nucleosome
        dummyNucleosome.SetIsNucleoAdjustable(false);

        AddCopyOfNucleosome(dummyNucleosome);

        GenerateStartLinker(GetNucleosome(0) );

        dummyNucleosome.SetIsNucleoAdjustable(true);

        // Loop on all the other nucleosomes and add them
        for(int i=0; i<23; ++i) // 23
        {
            // Translate the dummy back the the center
            dummyNucleosome.TranslateInLocalFrame(-xTranslation);

            // Rotate along the fiber Z axis
            //dummyNucleosome.RotateInFrame(fFiberDeltaAngle, QVector3D(0,0,1), GetModelMat() );
            dummyNucleosome.RotateInLocalFrame(fFiberDeltaAngle, QVector3D(0,0,1) );

            // Translate to the new position
            dummyNucleosome.TranslateInLocalFrame(xTranslation);

            // Advance towards the Z fiber axis
            dummyNucleosome.TranslateInLocalFrame(QVector3D(0,0, fFiberZShiftBtwNucl) );

            // Add a copy of the dummy to the container
            AddCopyOfNucleosome(dummyNucleosome);
        }

        GenerateEndLinker(GetNucleosome(GetNucleosomeNumber()-1) );

        // Set the generation flag to true
        fIsGenerated = true;
    }
}

void VoxelStraight::AnimateExtend(float factor)
{
    // Loop on all the nucleosomes without the first one
    for(int i=1, ie=fNucleosomeIndex.size(); i<ie; ++i)
    {
        Nucleosome& nucleosome = GetNucleosome(i);
        Nucleosome& previousNucleosome =  GetNucleosome(i-1);

        nucleosome.TranslateInFrame(QVector3D(0,0, i*factor/100), GetModelMat() );

        UpdateLinker(i-1, previousNucleosome, nucleosome);
        GenerateStartLinker(GetNucleosome(0) );
        GenerateEndLinker(GetNucleosome(fNucleosomeIndex.back() ) );
    }
}

}}}
