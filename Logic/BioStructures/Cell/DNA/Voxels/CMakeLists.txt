#add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/BioStructures")

add_sources(
   VVoxel.cc
   VoxelStraight.cc
)

add_headers(
   VVoxel.hh
   VoxelStraight.hh
)

add_uis()
