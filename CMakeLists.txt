#----------------------------------------------------------------------------
# Setup the project
#
cmake_minimum_required(VERSION 2.8)
project(OpenDnaFabric)

set(QT_MIN_VERSION "5.9.0")
set(SYMVOX_MIN_VERSION "1.0.0")

# Include the "utils" cmake file
include("${CMAKE_SOURCE_DIR}/cmake/utils.cmake")

# Check if c++11 can be enabled
TestIfCpp11()

#----------------------------------------------------------------------------
# Find packages
#
find_package(Qt5Core ${QT_MIN_VERSION} REQUIRED)
find_package(Qt5Widgets ${QT_MIN_VERSION} REQUIRED)
find_package(Qt5QuickWidgets ${QT_MIN_VERSION} REQUIRED)
find_package(Qt5Quick ${QT_MIN_VERSION} REQUIRED)
find_package(Qt5Qml ${QT_MIN_VERSION} REQUIRED)
find_package(Qt5Multimedia ${QT_MIN_VERSION} REQUIRED)
find_package(Qt5WebEngineWidgets ${QT_MIN_VERSION} REQUIRED)
find_package(Qt5WebEngine ${QT_MIN_VERSION} REQUIRED)

# Z_LIB is required for assimp to work
find_library(Z_LIB z  REQUIRED)

set(CMAKE_AUTOMOC ON)

#-----------------------------------------------------------------------------
# Add SymVox
#
find_package(SymVox ${SYMVOX_MIN_VERSION} REQUIRED)

add_subdirectory("${PROJECT_SOURCE_DIR}/App")
add_subdirectory("${PROJECT_SOURCE_DIR}/Logic")
#add_subdirectory("${PROJECT_SOURCE_DIR}/Shaders")
add_subdirectory("${PROJECT_SOURCE_DIR}/Data")
add_subdirectory("${PROJECT_SOURCE_DIR}/GUI")
add_subdirectory("${PROJECT_SOURCE_DIR}/Controller")

# Call the cmake files within those directories (they use the previously defined macros)
#
add_sources()
add_headers()
add_uis()
add_qml()
add_res()
add_copy()

# Add qml files to the resource system
add_res(${qml})

# Copy the files
CopyFiles(copy)

set(resource_file_name "app_resource")
WriteQrcFile(${resource_file_name} ${res})

# Dummy target just here to add the resource files to the project tree
add_custom_target(dummy SOURCES ${res}
                                "${resource_file_name}.qrc"
                                "README.md"
                                "doc/schema/runtime.qmodel"
                                "doc/schema/app_structure.qmodel"
                                )
#GenerateResourceBinary(${resource_file_name}) # Generate a resource binary different from the executable
CustomAddResources(RESOURCES "${resource_file_name}.qrc") # Add resources inside the executable


# Include the .hh file
#
include_directories(
                    ${Qt5Core_INCLUDE_DIRS}
                    ${Qt5Widgets_INCLUDE_DIRS}
                    ${Qt5QuickWidgets_INCLUDE_DIRS}
                    ${Qt5Quick_INCLUDE_DIRS}
                    ${Qt5Qml_INCLUDE_DIRS}
                    ${Qt5Multimedia_INCLUDE_DIRS}
                    ${Qt5WebEngineWidgets_INCLUDE_DIRS}
                    ${Qt5WebEngine_INCLUDE_DIRS}
                    ${PROJECT_SOURCE_DIR}
                    ${LIBSYMVOX_INCLUDE_DIRS}
                    )

#----------------------------------------------------------------------------
# Add the executable, and link it to the libraries
#

# this will run uic on .ui files:
QT5_WRAP_UI( UI_HDRS ${uis} )

# All headers for classes that contain Q_OBJECT macro must be processed by the meta-object compiler
QT5_WRAP_CPP(project_MOC ${headers_QOBJ} )

# Add compiler flags for building executables
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS}")

qt5_generate_moc(main.cc main.moc)

add_executable(${PROJECT_NAME} main.cc ${sources} ${headers}
                    ${project_MOC} ${UI_HDRS}
                    ${RESOURCES}
                    )

include_directories( ${CMAKE_BINARY_DIR} )
target_link_libraries(${PROJECT_NAME}   ${Qt5Core_LIBRARIES}
                                        ${Qt5Widgets_LIBRARIES}
                                        ${Qt5QuickWidgets_LIBRARIES}
                                        ${Qt5Quick_LIBRARIES}
                                        ${Qt5Qml_LIBRARIES}
                                        ${Qt5WebEngineWidgets_LIBRARIES}
                                        ${Qt5WebEngine_LIBRARIES}
                                        ${LIBSYMVOX_LIBRARIES}
                                        )

# Copy the data folder in the build directory
#file(COPY ${data} DESTINATION ${CMAKE_BINARY_DIR})

